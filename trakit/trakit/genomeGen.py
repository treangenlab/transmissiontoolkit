### IMPORTS ###
import os

from pathlib import Path
from trakit.trakitUtils import TransmissionPair
from trakit.vcfParsing import extract_variants_from_vcf

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

def get_seq(sequence):
    """
    Returns string representation of a genome sequence given a FASTA file.
    Assumes only one sequence in the file
    """    
    consensus = str()
    with open(sequence, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
        for line in lines[1:]:
            consensus += line
            if line and line[0] == '>':
                raise ValueError('File should only include one sequence.')
    return consensus

def build_majority_consensus(
    vcf_file, 
    reference, 
    masks=None, 
    mask_type='hide',
    parse_type='biallelic'
):
    """
    Constructs consensus sequence containing the majority variants.

    Parameters
    ----------
    vcf_file : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site

    Returns
    -------
    str
        Consensus sequence constructed from the VCF file and reference genome
    """  
    consensus = list(get_seq(reference))
    variants = extract_variants_from_vcf(
        vcf_file, 
        min_AF=.5, # formerly 0. changed to fix https://trello.com/c/9hiDyyYk/212-investigate-why-samples-w-identical-consensus-are-not-aligned-on-the-tree
        max_AF=1, 
        parse_type=parse_type, 
        store_reference=True, 
        masks=None, 
        mask_type='hide'
    )
    highest_variants = {}
    for pos in variants:
        highest_freq = 0
        kept_var = None
        for var in variants[pos]:
            if variants[pos][var][0] > highest_freq:
                highest_freq = variants[pos][var][0]
                kept_var = var
        if kept_var:
            highest_variants[pos] = str(kept_var)
    for pos in highest_variants:
        consensus[pos - 1] = highest_variants[pos]
    return ''.join(consensus)

def build_minor_consensus(
        vcf_file,
        reference,
        min_AF=0,
        max_AF=1, 
        masks=None,
        mask_type='hide',
        parse_type='multiallelic'
):
    """
    Constructs consensus sequence containing variants with the second highest 
    frequency.

    Parameters
    ----------
    vcf_file : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 

        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    
    Returns
    -------
    str
        Consensus sequence containing the second most frequent alleles
    """
    consensus = list(get_seq(reference))
    data = extract_variants_from_vcf(
        vcf_file, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        parse_type=parse_type, 
        store_reference=True, 
        masks=None, 
        mask_type='hide'
    )
    variants = {}
    for pos in data:
        highest_freq = 0
        max_freq = max([data[pos][var][0] for var in data[pos]])
        kept_var = None
        for var in data[pos]:
            freq = data[pos][var][0]
            if freq > highest_freq and freq != max_freq:
                highest_freq = freq
                kept_var = var
        if kept_var:
            variants[pos] = str(kept_var)
    for pos in variants:
        consensus[pos - 1] = variants[pos]
    return ''.join(consensus)

def vcf2fasta(
        vcf_path, 
        reference, 
        output_dir="", 
        line_length=LINE_WRAP, 
        min_AF=0,
        max_AF=1,
        parse_type='biallelic',
        masks=None,
        mask_type='hide',
        consensus_type='majority'
):
    """
    Writes a FASTA file containing a constructed consensus sequence.

    Parameters
    ----------
    vcf_path : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    output_dir : str, optional
        Path to a directory to save the generated FASTA file in
    line_length : int, optional
        Maximum number of characters per line allowed in the FASTA file
    min_AF : float, optional
        Minimum allele frequency to validate a variant
    max_AF : float, optional
        Maximum allele frequency to validate a variant
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    consensus_type : {'majority', 'minority'}, optional
        Whether to construct the consensus sequence containing majority or
        minority variants
    
    Raises
    ------
    ValueError
        When the consensus_type input is invalid
    """
    # If user specifies output directory
    if output_dir:
        # Make directory if it doesn't exist
        Path(output_dir).mkdir(parents=True, exist_ok=True)
    basename = os.path.basename(vcf_path).split('.')[0]
    path = os.path.join(output_dir, basename + '.consensus.fasta')
    if consensus_type == 'majority':
        seq = build_majority_consensus(
            vcf_path, 
            reference, 
            masks=None, 
            mask_type='hide',
            parse_type=parse_type
        )
    elif consensus_type == 'minor':
        seq = build_minor_consensus(
            vcf_path, 
            reference, 
            min_AF=0, 
            max_AF=1, 
            masks=None, 
            mask_type='hide',
            parse_type=parse_type
        )
    else:
        raise ValueError(f'Unexpected consensus_type: {consensus_type}.')
    with open(path, 'w') as f:
        f.write(f'>{basename}\n')
        for i in range(0, len(seq), line_length):
            f.write(seq[i: i+line_length] + '\n')