### IMPORTS ###
import os
import vcf

from trakit.trakitUtils import TransmissionPair, parse_mask_file
from pathlib import Path
from collections import defaultdict

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

def _is_vcf_file(path):
    """
    Checks whether the specified path is a file.

    Parameters
    ----------
    path : str
        Path to a file.
    """
    return path.endswith(".vcf")

def _is_valid_variant(min_read_depth, min_AF, max_AF, var_reads, freq):
    """
    Determine if a variant has a valid allele frequency and depth.

    Parameters
    ----------
    min_read_depth : int
        Minimum read depth to validate a variant
    min_AF : float
        Minimum allele frequency to validate a variant
    max_AF : float
        Maximum allele frequency to validate a variant
    var_reads : int
        Number of reads covering a variant
    freq : float
        allele frequency

    Returns
    -------
    boolean
        Whether a variant has a valid AF and depth
    """
    #If the allele passes restrictions, return True
    if var_reads >= min_read_depth and freq >= min_AF and freq <= max_AF:
        return True
    #Otherwise, return False
    return False

def find_empty_vcf_files(vcf_folder):
    """
    Identifies empty VCF files within the provided directory.

    Parameters
    ----------
    vcf_folder : str
        Path to a directory
    
    Returns
    -------
    str
        Path to the new folder
    list
        list of paths to empty VCF files
    """
    directory = "empty_vcfs"
    path = os.path.join(vcf_folder, directory)
    Path(path).mkdir(parents=True, exist_ok=True)
    empty_vcf_list = []
    for filename in os.listdir(vcf_folder):
        file_path = os.path.join(vcf_folder, filename)
        if _is_vcf_file(file_path):
            data = vcf.Reader(open(file_path, 'r'))
            counter = 0
            while counter == 0:
                for dummy in data:
                    counter += 1
                counter += 1
            if counter == 1:
                empty_vcf_list.append(file_path)
    return path, empty_vcf_list
        
# TODO(ahwang): use parse_type
def extract_variants_from_vcf(
        filename,
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type="biallelic",
        store_reference=True,
        masks=None,
        mask_type='hide'
):
    """
    Extracts variant data from VCF and creates a dictionary storing data
    in the form:

            {
                position: {
                    variant: [
                        frequency, 
                        depth
                    ]
                }
            }
    
    Parameters
    ----------
    filename : str
        Path to a VCF file
    min_read_depth : int
        Minimum number of reads required to support a variant, 
        useful for filtering potential errors
    min_AF : float
        Minimum frequency all variants must hold, useful for filtering
        potential errors
    max_AF : float
        Maximum frequency to include in parse, useful for extracting 
        low frequency variants
    masks : str
        A txt file with a list of erroneous positions to mask
    parse_type : {'biallelic', 'multiallelic'} (unused)
        Whether to handle up to 2 or multiple variants per site
    store_reference: boolean
        Whether to store reference allele data 
    masks : str
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    
    
    Returns
    -------
    dict
        Mapping containing variant information

                {
                    position: {
                        variant: [
                            frequency of variant among all reads at this position, 
                            number of reads with this variant
                        ]
                    },
                    ...
                }

    Raises
    ------
    ValueError
        When one of the inputs is invalid
    """
    PARSE_TYPES = {"biallelic", "multiallelic"}
    MASK_TYPES = {"hide", "highlight"}
    if min_read_depth < 0 or int(max_AF) > 1 or parse_type not in PARSE_TYPES or str(mask_type) not in MASK_TYPES:
        raise ValueError("Invalid input.")
    if masks != None:
        mask_positions = parse_mask_file(masks)
    #lfv_data = Biallelic() if parse_type == "biallelic" else Multiallelic()
    lfv_data = {}
    data = vcf.Reader(open(filename, 'r'))
    # reference data. { position: {first reference base: [% reads with matching base, read depth}] }
    ref_data = {}
    for record in data:
        # Amount of reads at particular location
        raw_depth = record.INFO['DP']
        pos = record.POS
        variant = str(record.ALT[0])
        ref = str(record.REF[0])
        # Check if DP4 is one of the fields. 
        if 'DP4' in record.INFO:
            # Amount of reads supporting the variant
            var_depth = record.INFO['DP4'][2] + record.INFO['DP4'][3]
            freq = float(var_depth / raw_depth)
        else:
            var_depth = record.INFO["DP"]
            freq = record.INFO["AF"]		    
        #doesn't include masked positions based on user settings
        if masks != None and pos in mask_positions and mask_type == 'hide':
            continue
        # If the variant passes restrictions, store data
        # TODO(ahwang): does record.INFO["AF"] = var_depth / raw_depth when DP4 is not present?
        if _is_valid_variant(min_read_depth, min_AF, max_AF, var_depth, freq):
            #lfv_data.store(pos, var, freq, var_depth)
            if pos not in lfv_data:
                lfv_data[pos] = {variant: [freq, var_depth]}
            else:
                lfv_data[pos].update({variant: [freq, var_depth]})
        if store_reference and pos not in ref_data:
            # Calculate how many reads match the reference at this location
            # aka the number of reads with allele at this position that matches the reference
            if 'DP4' in record.INFO:
                ref_depth = record.INFO['DP4'][0] + record.INFO['DP4'][1]
                ref_freq = ref_depth/raw_depth
            else:
                ref_depth = record.INFO["DP"]
                ref_freq = 1 - record.INFO["AF"]			       
            # If ref allele passes restrictions, store the data
            if _is_valid_variant(min_read_depth, min_AF, max_AF, ref_depth, ref_freq):
                ref_data[pos] = {ref: [ref_freq, ref_depth]}
    # After parsing is complete, make object into a dictionary
    #lfv_data = dict(lfv_data)
    # If we collected reference data, update lfv_data
    if store_reference:
        for pos in ref_data:
            if pos not in lfv_data:
                lfv_data[pos] = ref_data[pos]
            else:
                lfv_data[pos].update(ref_data[pos])
    return lfv_data

def extract_shared_variants(
        donor,
        recip,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type="multiallelic",
        store_reference=True,
        masks=None,
        mask_type="hide",
        store_donor_only_variants=False
):
    """
    Parses a pair of VCF files to find common variants.

    Parameters
    ----------
    donor : str
        Path to a VCF file
    recip : str
        Path to a VCF file
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    store_reference : boolean, optional
        Whether to store data representing reads that match the reference
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    store_donor_only_variants : boolean, optional
        Whether to include variants found only in the donor sample, in the result.

    Returns
    -------
    TransmissionPair
        TransmissionPair instance representing the variants found in the
        provided donor and recipient samples.
    """
    donor_file = donor
    recip_file = recip
    if type(min_AF) is tuple:
        donor_min_AF, recip_min_AF = min_AF
    else:
        donor_min_AF = min_AF
        recip_min_AF = min_AF
    if type(max_AF) is tuple:
        donor_max_AF, recip_max_AF = max_AF
    else:
        donor_max_AF = max_AF
        recip_max_AF = max_AF
    donor_data = extract_variants_from_vcf(
        donor_file,
        min_read_depth=min_read_depth,
        max_AF=donor_max_AF, # formerly .5
        min_AF=donor_min_AF,
        parse_type=parse_type,
        store_reference=store_reference,
        masks=masks,
        mask_type=mask_type)
    recip_data = extract_variants_from_vcf(
        recip_file,
        min_read_depth=min_read_depth,
        max_AF=recip_max_AF, # formerly 1
        min_AF=recip_min_AF,
        parse_type=parse_type,
        store_reference=store_reference,
        masks=masks,
        mask_type=mask_type)
    shared_count = 0
    # Stored as {pos: {var: [donor freq., recip. freq]}} bc Maria had two bb input files 
    # and one required all this info, might change later tho
    bb_data = defaultdict(dict)
    if masks:
        mask_list = parse_mask_file(masks)
    # Iterate through each variant at each position
    for pos in donor_data: 
        if masks and pos in mask_list and mask_type == "hide":
            continue
        for var in donor_data[pos]:
            # Store donor data
            donor_freq = donor_data[pos][var][0]
            donor_depth = donor_data[pos][var][1]
            if not store_donor_only_variants: 
                if pos not in recip_data or var not in recip_data[pos]:
                    continue
            if pos not in bb_data:
                bb_data[pos] = {var: [donor_freq, 0.0, donor_depth, 0.0]}
            else:
                bb_data[pos].update({var: [donor_freq, 0.0, donor_depth, 0.0]})
            # If recipient has same variant at same location, store it
            # TODO(ahwang): Why is .02 and .5 hardcoded??
            # TODO(ahwang): Check that recip_freq passes allele freq requirements
            # TODO(ahwang): Check that read depths for both samples pass the min_read_depth 
            # if pos in recip_data and var in recip_data[pos] and donor_data[pos][var][0] > 0.02 and donor_data[pos][var][0] <= 0.5:
            if pos in recip_data and var in recip_data[pos]:
                recip_freq = recip_data[pos][var][0]
                recip_depth = recip_data[pos][var][1]
                bb_data[pos][var][1] = recip_freq
                bb_data[pos][var][3] = recip_depth
                shared_count += 1
    donor_basename = os.path.basename(donor_file).split(".")[0]
    recip_basename = os.path.basename(recip_file).split(".")[0]
    return TransmissionPair(donor_basename, recip_basename, bb_data, shared_count)

def extract_shared_variants_all_pairs(
        vcf_path,
        masks=None,
        mask_type='hide',
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type='biallelic'
):
    """
    Finds common variants among all possible pairs of VCF files in a directory.

    Parameters
    ----------
    vcf_path : str
        Path to a directory of VCF files
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 10
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    
    Returns
    -------
    List
        List of TransmissionPair instances
    """
    all_pairs = []
    vcf_folder = Path(vcf_path)
    for donor_filename in os.listdir(vcf_folder):
        for recipient_filename in os.listdir(vcf_folder):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                donor_path = os.path.join(vcf_folder, donor_filename)
                recipient_path = os.path.join(vcf_folder, recipient_filename)
                all_pairs.append(
                    extract_shared_variants(
                        donor_path, 
                        recipient_path, 
                        min_read_depth=min_read_depth, 
                        min_AF=min_AF, 
                        max_AF=max_AF, 
                        parse_type=parse_type, 
                        masks=masks, 
                        mask_type=mask_type
                    )
                )
    return all_pairs