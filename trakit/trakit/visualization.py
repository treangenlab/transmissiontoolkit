### IMPORTS ###
import os
import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

from trakit.vcfParsing import _is_vcf_file, extract_shared_variants
from trakit.bbAnalysis import _estimate_bottleneck_size
from trakit.trakitUtils import *
from trakit.PhyloTree import PhyloTree
from pathlib import Path
from collections import defaultdict

# Using package resources
import importlib
import importlib.resources as resources

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

def plot_bottleneck_in_clusters(
        vcf_directory,
        metadata_path,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        min_shared=1,
        save=True,
        output_folder=None,
        verbose=False,
        masks=None,
        legend_loc="upper left"
):
    """
    Plot bottleneck size of sample pairs belonging to the same cluster.
    see all_pairs for parameter definitions.

    Parameters
    ----------
    vcf_directory : str
        Path to a directory of VCF files
    metadata_path : str
        Path to a TSV file labeling samples
        with metadata. If metadata is provided, the bottleneck size will only be
        estimated for pairs in the same cluster as defined in the metadata.
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int, optional
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the visualization as a PNG file
    output_folder : str, optional
        Path to folder to store the generated visualization PNG file in
    verbose : boolean, optional
        Whether to print the command(s) being executed
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    legend_loc : str or pair of floats, optional
        Location of the legend. See matplotlib.pyplot.legend documentation for 
        acceptable values (ex. 'upper left', 'upper right')
    Returns
    -------
    List 
        Maps pair ID (integer) to the corresponding pair of samples.
    """
    metadata_cluster_assignments = {}
    metadata_clusters = defaultdict(list)
    file = open(metadata_path)
    read_tsv = csv.reader(file, delimiter="\t")
    next(read_tsv) # Assume column names are present
    for line in read_tsv:
        basename = line[0].strip()
        group_id = line[1]
        metadata_cluster_assignments[basename]=group_id
        metadata_clusters[group_id].append(basename)
    # Use dictionaries to group entries by cluster.
    bottleneck_sizes, left_bound_intervals, right_bound_intervals, pair_nums = defaultdict(list), defaultdict(list), defaultdict(list), defaultdict(list)
    pair_num = 1
    sample_pairs = [set()]
    all_pairs_input = []
    for donor_filename in os.listdir(vcf_directory):
        for recipient_filename in os.listdir(vcf_directory):
            donor_basename = donor_filename.split(".")[0]
            recipient_basename = recipient_filename.split(".")[0]
            if donor_filename != recipient_filename and \
                donor_basename in metadata_cluster_assignments and \
                recipient_basename in metadata_cluster_assignments and \
                metadata_cluster_assignments[donor_basename] == metadata_cluster_assignments[recipient_basename]:
                donor_path = os.path.join(vcf_directory, donor_filename)
                recipient_path = os.path.join(vcf_directory, recipient_filename)
                # Estimate the transmission bottleneck size.
                bottleneck_estimate = _estimate_bottleneck_size(
                    donor_path, 
                    recipient_path, 
                    min_read_depth, 
                    min_AF, 
                    max_AF, 
                    min_shared, 
                    verbose=verbose, 
                    cluster_name=metadata_cluster_assignments[recipient_basename],
                    plot_bool=False,
                    masks=masks
                )
                sample_names = set([donor_basename, recipient_basename])
                if bottleneck_estimate:
                    all_pairs_input.append(bottleneck_estimate)
                    sample_pairs.append(sample_names)
    all_pairs_input.sort(key=lambda x:x[0])
    pair_num = 1
    for read in all_pairs_input:
        cluster_name = read[0]
        bottleneck_sizes[cluster_name].append(read[1])
        left_bound_intervals[cluster_name].append(read[2])
        right_bound_intervals[cluster_name].append(read[3])
        pair_nums[cluster_name].append(pair_num)
        pair_num += 1
    plt.figure(figsize=(10,8))
    N = len(metadata_clusters)
    cmap = plt.cm.get_cmap("gist_rainbow", N+1)
    i = 0
    # The pair_nums mapping values are in chronological order.
    for group_id in pair_nums.keys():
        color = cmap(i)
        plt.errorbar(pair_nums[group_id],bottleneck_sizes[group_id], np.asarray([left_bound_intervals[group_id],right_bound_intervals[group_id]]), ls = "None", color = color, label=group_id)
        plt.scatter(pair_nums[group_id],bottleneck_sizes[group_id],color=color, s=32,marker="o")
        i += 1
    xtick_fontsize = 24 if pair_num < 11 else (18 if pair_num < 21 else 12)
    ytick_fontsize = 24 if pair_num < 11 else 20
    plt.xlabel('Pair ID', fontweight='bold', fontsize=26)
    plt.ylabel('Bottleneck Size', fontweight='bold', fontsize=26)
    plt.legend(title="Cluster", loc=legend_loc)
    plt.tight_layout()
    plt.yticks(fontsize = ytick_fontsize)
    plt.xticks(range(1, pair_num), range(1, pair_num), fontsize = xtick_fontsize)
    plt.show()
    if save:
        if output_folder == None:
            path = os.path.join(vcf_directory, 'bottleneck')
        else:
            path = os.path.join(output_folder, 'bottleneck')
        filename = 'bottleneck_chart.png'
        Path(path).mkdir(parents=True, exist_ok=True)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')
    return sample_pairs

def plot_bottleneck_all_pairs(
        vcf_directory,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        min_shared=1,
        save=True,
        output_folder=None,
        verbose=False,
        masks=None
):
    """
    Plot bottleneck size of all pairs of samples whose number of shared variants
    exceed a certain threshold

    Parameters
    ----------
    vcf_directory : str
        Path to a directory of VCF files
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int, optional
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the visualization as a PNG file
    output_folder : str, optional
        Path to folder to store the generated bottleneck size chart PNG file in
    verbose : boolean, optional
        Whether to print the command(s) being executed
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
   
    Returns
    -------
        list 
            Maps pair ID (list index, starting at 1) to the corresponding pair of samples.
    """
    bottleneck_sizes, left_bound_intervals, right_bound_intervals, pair_nums = [], [], [], []
    pair_num = 1
    sample_pairs = [set()]
    all_pairs_input = []
    for donor_filename in os.listdir(vcf_directory):
        for recipient_filename in os.listdir(vcf_directory):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                donor_path = os.path.join(vcf_directory, donor_filename)
                recipient_path = os.path.join(vcf_directory, recipient_filename)
                # Estimate the transmission bottleneck size.
                bottleneck_estimate = _estimate_bottleneck_size(
                    donor_path, 
                    recipient_path, 
                    min_read_depth, 
                    min_AF, 
                    max_AF, 
                    min_shared, 
                    verbose=verbose,
                    plot_bool=False,
                    masks=masks
                )
                if bottleneck_estimate:
                    all_pairs_input.append(bottleneck_estimate)
                    donor = donor_filename.split(".")[0] 
                    recipient = recipient_filename.split(".")[0]
                    sample_names = set([donor, recipient])
                    sample_pairs.append(sample_names)	
    # Sort pairs by bottleneck size.
    all_pairs_input.sort(key=lambda x:int(x[1]))
    pair_num = 1
    for read in all_pairs_input:
        bottleneck_sizes.append(read[1])
        left_bound_intervals.append(read[2])
        right_bound_intervals.append(read[3])
        pair_nums.append(pair_num)
        pair_num += 1  
    plt.figure(figsize=(10,8))
    plt.errorbar(pair_nums,bottleneck_sizes, np.asarray([left_bound_intervals,right_bound_intervals]), ls = "None", color = "gray")
    plt.scatter(pair_nums,bottleneck_sizes,c='blue', s=32,marker="o")
    plt.xlabel('Pair ID', fontweight='bold',fontsize=26)
    plt.ylabel('Bottleneck Size', fontweight='bold',fontsize=26)
    plt.tight_layout()
    plt.yticks(fontsize = 24)
    plt.xticks(range(1, pair_num), range(1, pair_num), fontsize = 24)
    plt.show()
    if save:
        if output_folder == None:
            path = os.path.join(vcf_directory, 'bottleneck')
        else:
            path = os.path.join(output_folder, 'bottleneck')
        filename = 'bottleneck_chart.png'
        Path(path).mkdir(parents=True, exist_ok=True)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')
    return sample_pairs

def plot_allele_frequencies_all_pairs(
        vcf_path,
        plot_type='unweighted',
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type='biallelic',
        min_shared=4,
        save=True,
        output_path=None,
        masks=None,
        mask_type='hide'
):
    """
    Generates an allele frequency bar plot for every possible pair of samples.

    Parameters
    ----------
    vcf_path : str
        Path to a directory of VCF files
    plot_type : {'unweighted', 'weighted'}, optional
        Whether to weight the thickness of bars by read depth
    min_read_depth : int, optional
        Minimum number of reads required to support a variant
    min_AF : float 
        Minimum allele frequency to plot a variant
    max_AF : float 
        Maximum allele frequency to plot a variant
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    min_shared : int
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the bar plots as PNG files
    output_folder : str, optional
        Path to folder to store the generated PNG files in
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    """
    vcf_folder = Path(vcf_path)
    #shown = 0
    for donor_filename in os.listdir(vcf_folder):
        for recipient_filename in os.listdir(vcf_folder):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                # Only create a bar plot if the donor-recipient pair shares a minimum number of variants
                donor_path = os.path.join(vcf_folder, donor_filename)
                recipient_path = os.path.join(vcf_folder, recipient_filename)
                transm_pair = extract_shared_variants(donor_path, recipient_path)
                if transm_pair.num_shared > min_shared:
                    plot_allele_frequencies(
                        donor_path, 
                        recipient_path, 
                        min_read_depth=min_read_depth, 
                        min_AF=min_AF,
                        max_AF=max_AF, 
                        save=save, 
                        output_folder=output_path, 
                        parse_type=parse_type, 
                        store_reference=True,  # TODO(ahwang): Make this a parameter?
                        plot_type=plot_type, 
                        masks=masks, 
                        mask_type=mask_type)
                    # TODO: What is the reasoning behind only showing the first 5? Was this just for testing?
                    # TODO: This check is ineffective because make_bar_plot displays the plot upon creation.
                    # if shown <= 5:
                    # 	plt.show()
                    # 	shown += 1

def plot_allele_frequencies(
        donor_path,
        recipient_path,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        save=True,
        output_folder=None,
        parse_type="biallelic",
        store_reference=True,
        plot_type='unweighted',
        masks=None,
        mask_type='hide'
):
    """
    Generate an allele frequency bar plot for a pair of samples

    Parameters
    ----------
    donor_path : str
        Path to a VCF file
    recipient_path : str
        Path to a VCF file
    min_read_depth : int
        Minimum read depth to plot a variant
    min_AF : float 
        Minimum allele frequency to plot a variant
    max_AF : float 
        Maximum allele frequency to plot a variant
    save : boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder: str, optional
        Path to the folder in which the allele frequency chart will be saved. 
    parse_type: str, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    store_reference : boolean, optional
        Whether to include reference alleles in the plot
    plot_type: {'unweighted', 'weighted'}, optional
        Whether the thickness of bars should be weighted by read depth
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    """
    donor_basename = os.path.basename(donor_path).split(".")[0]
    recipient_basename = os.path.basename(recipient_path).split(".")[0]
    PARSE_TYPES = {"biallelic", "multiallelic"}
    PLOT_TYPES = {"unweighted", "weighted"}
    MASK_TYPES = {"hide", "highlight"}
    if parse_type not in PARSE_TYPES or plot_type not in PLOT_TYPES or mask_type not in MASK_TYPES:
        raise ValueError("Invalid input.")
    transm_pair = extract_shared_variants(
        donor_path, 
        recipient_path, 
        min_read_depth=min_read_depth, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        parse_type=parse_type, 
        store_reference=store_reference, 
        masks=masks, 
        mask_type=mask_type)
    variants = transm_pair.variants
    if len(variants) == 0:
        return None
    positions, donor_freqs, recipient_freqs, donor_depths, recipient_depths = [], [], [], [], []
    # Create plot labels
    donor_label = str('Donor: ' + donor_basename)
    recipient_label = str('Recipient: ' + recipient_basename)
    max_read = 0
    min_read = float("inf")
    for pos in sorted(variants):
        # TODO: (nit) change "nuc" to "var" for consistency
        for nuc in variants[pos]:
            positions.append((pos, nuc))
            donor_freqs.append(round(variants[pos][nuc][0], 4))
            recipient_freqs.append(round(variants[pos][nuc][1], 4))
            donor_depths.append((variants[pos][nuc][2]))
            recipient_depths.append((variants[pos][nuc][3]))
    # TODO: only compute these values and textstr if the plot_type is weighted
    max_read = max(x for x in donor_depths + recipient_depths if x != 0)
    min_read = max(0, min(x for x in donor_depths + recipient_depths if x != 0))       
    width_divider = max_read*3
    donor_depths = [x/width_divider for x in donor_depths]
    recipient_depths = [x/width_divider for x in recipient_depths]
    textstr = str("Allele Frequencies   Max Depth: " + str(int(max_read)) + " Min Depth: " + str(int(min_read)))
    # the label locations
    x = np.arange(len(positions)) 
    # the width of the bars
    width = 0.35  
    fig, ax = plt.subplots()
    if plot_type == 'unweighted':
        ax.bar(x - width/2, donor_freqs,  color='#7f6d5f', width=width,
            edgecolor='white', label=donor_label)
        ax.bar(x + width/2, recipient_freqs, color='#557f2d', width=width,
            edgecolor='white', label=recipient_label)
    if plot_type == 'weighted':
        ax.bar(x - width/2, donor_freqs,  color='#7f6d5f', width=donor_depths,
            edgecolor='white', label=donor_label)
        ax.bar(x + width/2, recipient_freqs, color='#557f2d', width=recipient_depths,
            edgecolor='white', label=recipient_label) 
    # Add some text for labels, title and custom x-axis tick labels, etc.
    if plot_type == 'weighted':
        ax.set_ylabel(textstr)
    else:
        ax.set_ylabel("Allele Frequencies")
    # ax.set_xticks(x)
    # ax.set_xticklabels(positions)
    # ax.tick_params(axis = 'x', rotation = 50)
    ax.legend()
    maxn = len(positions)
    fig.tight_layout() 
    plt.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(8))
    plt.xlim(-0.5,maxn-0.5)
    plt.xticks(ticks=x, labels=positions, rotation=50)
    plt.tight_layout()
    #save = os.path.join(output_folder, filename)
    #plt.savefig(save, dpi=300, bbox_inches='tight')
    # TODO: Uncommenting this does not prevent all the plots from showing.
    #plt.show()
    # Download the image only if the save flag is turned on
    if save:
        folder = str(plot_type + "_bar_plots")
        if output_folder != None:
            path = os.path.join(output_folder, folder)
        else:
            path = folder
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = '%s_%s_allele_freq.png'%(donor_basename, recipient_basename)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')
        
def plot_shared_variant_counts(transm_pairs_list, mask_pairs_list, high_shared=6, save=True, output_folder=None):
    """
    Create chart showing number of sample pairs sharing a certain number of 
    variants when genome is both masked and unmasked. Use the outputs of
    count_shared_variants() as inputs.
    
    Parameters
    ----------
    transm_pairs_list : list
        list of TransmissionPair instances
    mask_pairs_list : list, optional
        list of TransmissionPair instances after masking
    high_shared : int, optional
        minimum number of variants a pair of samples must share to be "important"
    save: boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder : str, optional
        Path to the folder in which the masked shared variants chart will be saved. 
    """
    # Counts the number shared variants in all possible pairs of samples
    sv_count, important_pairs = count_shared_variants(
        transm_pairs_list, 
        mask_pairs_list=mask_pairs_list, 
        high_shared=high_shared)
    shared_variants = sorted(sv_count.items())
    sv, masked_pairs, complete_pairs, cell_text, columns, masked, complete= [], [], [], [], [], [], []
    for num in shared_variants:
        sv.append(num[0])
        complete_pairs.append(num[1][0])
        complete.append(num[1][0])
        masked_pairs.append(num[1][1])
        masked.append(num[1][1])
    cell_text.append(masked)
    cell_text.append(complete)
    columns = tuple(sv)
    rows = ['Masked Genome', 'Complete Genome']
    colors = ['blue', 'red']
    masked_label = "Masked Genome"
    complete_label = "Complete Genome"
    x = np.arange(len(sv))  # the label locations
    width = 0.35  # the width of the bars
    plt.figure(figsize=(10,6))
    plt.bar(x - width/2, masked_pairs,  color='blue', width=width,
        edgecolor='white', label=masked_label)
    plt.bar(x + width/2, complete_pairs, color='red', width=width,
        edgecolor='white', label=complete_label)
    plt.table(cellText=cell_text, rowLabels=rows, rowColours=colors, colLabels=columns)
    # Add some text for labels, title and custom x-axis tick labels, etc.
    maxn = len(sv)
    plt.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(12))
    plt.xlim(-0.5,maxn-0.5)
    plt.xticks([])
    plt.ylabel("Number of Pairs")
    plt.xlabel("Number of Shared Variants", labelpad=50)
    # Download the image only if the save flag is turned on
    if save:
        folder = "msv"
        if output_folder != None:
            path = os.path.join(output_folder, folder)
        else:
            path = folder
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = 'msv_chart'
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')

# TODO: Should a mask file always be supplied? What if the user just wants to view top 20 
# most common positions, unmasked?
def plot_top_shared_variant_sites(
        transmission_pairs,
        mask_file,
        input_folder,
        save=True,
        output_folder=None,
        shown_variants=20
):
    """
    Creates chart showing top positions that have the most pairs with a variant at that position

    Parameters
    ----------
    transmission_pairs : List
        List of TransmissionPair instances
    mask_file : str
        Path to a txt file containing comma-separated genomic sites to mask
    input_folder : str
        Path to a folder containing VCFs
    save : boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder: str, optional
        Path to the folder in which the chart will be saved. 
    shown_variants : int, optional
        Number of most common variants to plot
    """
    def barplot_properties():
        return {
            'xloc': [],
            'site': [],
            'frequency': [],
        }
    position_count = position_counter(transmission_pairs)
    mask_positions = parse_mask_file(mask_file)
    sorted_pos = sorted(position_count.items(), key=lambda x: x[1], reverse=True)
    barplot_data  = defaultdict(barplot_properties)
    sorted_pos = sorted_pos[0:shown_variants]
    position_data = [pos[0] for pos in sorted_pos]
    x = np.arange(len(sorted_pos))  # the label locations
    for idx, pos in zip(x, sorted_pos):
        if pos[0] in mask_positions:
            category = 'masked'
        else:
            category = 'unmasked'
        barplot_data[category]['xloc'].append(idx)
        barplot_data[category]['site'].append(pos[0])
        barplot_data[category]['frequency'].append(pos[1]) # number of pairs
    width = 0.35  # the width of the bars
    fig, ax = plt.subplots()
    masked_label = "Masked Site"
    complete_label = "Unmasked Site" 
    ax.bar(
        barplot_data['masked']['xloc'],
        barplot_data['masked']['frequency'],
        color='red',
        width=width,
        label=masked_label
    )  
    ax.bar(
        barplot_data['unmasked']['xloc'],
        barplot_data['unmasked']['frequency'], 
        color='blue',
        width=width,
        label=complete_label
    )  
    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Number of Pairs')
    ax.set_xlabel("Variant Position")
    ax.set_xticks(x)
    ax.set_xticklabels(position_data)
    ax.tick_params(axis = 'x', rotation = 50)
    fig.tight_layout()  
    # Using patches is necessary because the legend colors might not be
    # correct if the plot doesn't include either a masked or unmasked site.
    red_patch = mpatches.Patch(color='red', label=masked_label)
    blue_patch = mpatches.Patch(color='blue', label=complete_label)
    plt.legend(loc='lower left', bbox_to_anchor= (.15, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(12), handles= [red_patch, blue_patch])
    maxn = len(position_data)
    plt.xlim(-0.5,maxn-0.5)
    plt.tight_layout() 
    plt.show()
    if save:  
        if output_folder != None:
            path = os.path.join(output_folder, 'positions')
        else:
            path = os.path.join(input_folder, 'positions')
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = 'positions_chart'
        save = os.path.join(path, filename)
        fig.savefig(save, dpi=300, bbox_inches='tight')

def plot_phylotree_all(
        vcf_directory, 
        reference_fasta, 
        metadata_path=None,
        colors=COLORS, 
        output_dir='TransmissionViz', 
        min_AF=0,
        render_type=None,
        width=500,
        height=None,
        threads=THREADS,
        masks=None,
        mask_type='hide'
):
    """
    Generate figures on the full tree and subtree phylogenies given
    a directory of VCF files and a reference genome.

    It should be noted that running this provides less customizablity than calling the
    PhyloTree class methods individually.

    Parameters
    ----------
    vcf_directory : str
        Path to directory containing VCF files
    reference_fasta : str
        Path to reference genome FASTA file
    metadata_path : str, optional
        Path to a TSV file labeling samples with metadata
    colors : list, optional
        List of color strings to color nodes with
    output_dir : str, optional
        Path to directory to place all outputs in
    min_AF : float, optional
        Minimum allele frequency to detect a variant (SNP or iSNV)
    render_type : {None, "html", "svg"}, optional
        Type of file to save the tree plot as
    width : int or None, optional
        Width of the tree plots in pixels
    height : int or None, optional
        Height of the tree plots in pixels 
    threads : int, optional
        Number of threads to use when running Parsnp and RAxML
    masks : str, optional (unused)
        TODO(ahwang): Complete masks defn
    mask_type : str, optional (unused)
        TODO(ahwang): Complete mask_type defn
    """

    # Check if path exists
    if not os.path.exists(vcf_directory):
        raise FileNotFoundError(f"Directory does not exist: {vcf_directory}")
    if not os.path.exists(reference_fasta):
        raise FileNotFoundError(f"File does not exist: {reference_fasta}")
    
    # Run normal tree stuff
    tree = PhyloTree.PhyloTree(output_dir, reference_fasta, vcf_directory)
    # Group samples based on DNA sequence: map the base name => group number
    if metadata_path:
        tree.make_metadata_clusters(metadata_path, True)

    consensus_fasta_folder = tree.create_consensus_sequences()
    raxml_newick, refname = tree.infer_phylogeny(
        consensus_fasta_folder, 
        os.path.join(output_dir, "consensus"),
        threads=threads) # 1
    
    rtre = tree.construct_tree(tree_path=raxml_newick, root_wildcard=refname)
    tree.plot_tree(rtre, render_type=render_type, width=width, height=height)
    tree.plot_snp_heatmap(rtre, min_AF=min_AF, render_type=render_type)

    # Create iSNV-embedded sequence FASTA files. Align them.
    isnv_fasta_folder = tree.create_isnv_embedded_sequences()
    raxml_newick_isnv, refname = tree.infer_phylogeny(
        isnv_fasta_folder, 
        os.path.join(output_dir, "isnv"),
        threads=threads)
    rtre2 = tree.construct_tree(tree_path = raxml_newick_isnv, root_wildcard=refname)
    tree.plot_isnv_heatmap(rtre2, min_AF=min_AF, render_type=render_type)

    # Run RAxML on each iSNV cluster.
    cluster_folder_path = tree.align_isnv_clusters(threads=threads)

    # Visualize the clusters
    tree.plot_isnv_clusters(cluster_folder_path, min_AF=min_AF, render_type=render_type)

    # Get rid of tmp directory
    #shutil.rmtree(tmp_path, ignore_errors=True)        
############# CLASSES - MAKE NEW FILE??? ##############
