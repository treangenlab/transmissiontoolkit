import subprocess
from subprocess import Popen, PIPE
import ntpath
import os
import shutil
import glob
import datetime
import errno
from pathlib import Path

import vcf
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import toytree
import toyplot
import toyplot.svg

# PhyloTree imports
from collections import defaultdict
import csv
import math

# Using package resources
import importlib
import importlib.resources as resources

# Minimum allele frequency to detect an iSNV
MIN_AF = .02

def _is_vcf_file(path):
    """
    Checks whether the specified path is a file.

    Parameters
    ----------
    path : str
        Path to a file.
    """
    return path.endswith(".vcf")

def _add_executable_path(name):
    """
    Returns path to the trakit package's executable if `name` is not on $PATH.

    Parameters
    ----------
    name : str
        Name of the executable, ex. "raxmlHPC"
    
    Returns
    -------
    str
        Path to the executable in the package resources if it is not on $PATH.
        Otherwise, the original "name" input.
    """
    if shutil.which(name) is None:
        with resources.path('trakit.executables', name) as executable:
            return executable
    return name

def find_empty_vcf_files(vcf_folder):
    """
    Identifies empty VCF files within the provided directory.

    Parameters
    ----------
    vcf_folder : str
        Path to a directory
    
    Returns
    -------
    str
        Path to the new folder
    list
        list of paths to empty VCF files
    """
    directory = "empty_vcfs"
    path = os.path.join(vcf_folder, directory)
    Path(path).mkdir(parents=True, exist_ok=True)
    empty_vcf_list = []
    for filename in os.listdir(vcf_folder):
        file_path = os.path.join(vcf_folder, filename)
        if _is_vcf_file(file_path):
            data = vcf.Reader(open(file_path, 'r'))
            counter = 0
            while counter == 0:
                for dummy in data:
                    counter += 1
                counter += 1
            if counter == 1:
                empty_vcf_list.append(file_path)
    return path, empty_vcf_list

def move_empty_files(path, empty_vcf_list):
    """Move a provided list of VCF files to a directory."""
    for filename in empty_vcf_list:
        shutil.move(filename, path)


def _is_valid_variant(min_read_depth, min_AF, max_AF, var_reads, freq):
    """
    Determine if a variant has a valid allele frequency and depth.

    Parameters
    ----------
    min_read_depth : int
        Minimum read depth to validate a variant
    min_AF : float
        Minimum allele frequency to validate a variant
    max_AF : float
        Maximum allele frequency to validate a variant
    var_reads : int
        Number of reads covering a variant
    freq : float
        allele frequency

    Returns
    -------
    boolean
        Whether a variant has a valid AF and depth
    """
    #If the allele passes restrictions, return True
    if var_reads >= min_read_depth and freq >= min_AF and freq <= max_AF:
        return True
    #Otherwise, return False
    return False

def parse_mask_file(masks):
    """
    Parses a text file of genomic positions to mask.

    Parameters
    ----------
    masks : str
        Path to a text file of comma-separated integers
    
    Returns
    -------
    list
        list of integers read from the file
    """
    mask_positions = []
    with open(masks, "r") as mask_file:
            for line in mask_file:
                comma_split = line.split(",")
                for item in comma_split:
                    nums = item.split("-")
                    if len(nums) == 1:
                        mask_positions.append(int(nums[0]))
                    else:
                        for num in range(int(nums[0]), int(nums[1])):
                            mask_positions.append(int(num))
    return mask_positions

class TransmissionPair:
    """
    Representation of a pair of samples.

    Attributes
    ----------
    donor_basename : str
        Base name of the donor sample file
    recipient_basename : str
        Base name of the recipient sample file
    variants : dict
        Variant frequency and depth information

            {
                site (int): {
                    variant (str): [
                        donor frequency (float), 
                        recipient frequency (float), 
                        donor depth (int), 
                        recipient depth (int)
                    ]
                }
            }

    num_shared : int
        Number of variants shared by the pair
    """
    def __init__(self, donor_basename, recipient_basename, variants, num_shared):
        self.donor_basename = donor_basename
        self.recipient_basename = recipient_basename
        self.variants = variants
        self.num_shared = num_shared

# TODO(ahwang): use parse_type
def extract_variants_from_vcf(
        filename,
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type="biallelic",
        store_reference=True,
        masks=None,
        mask_type='hide'
):
    """
    Extracts variant data from VCF and creates a dictionary storing data
    in the form:

            {
                position: {
                    variant: [
                        frequency, 
                        depth
                    ]
                }
            }
    
    Parameters
    ----------
    filename : str
        Path to a VCF file
    min_read_depth : int
        Minimum number of reads required to support a variant, 
        useful for filtering potential errors
    min_AF : float
        Minimum frequency all variants must hold, useful for filtering
        potential errors
    max_AF : float
        Maximum frequency to include in parse, useful for extracting 
        low frequency variants
    masks : str
        A txt file with a list of erroneous positions to mask
    parse_type : {'biallelic', 'multiallelic'} (unused)
        Whether to handle up to 2 or multiple variants per site
    store_reference: boolean
        Whether to store reference allele data 
    masks : str
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    
    
    Returns
    -------
    dict
        Mapping containing variant information

                {
                    position: {
                        variant: [
                            frequency of variant among all reads at this position, 
                            number of reads with this variant
                        ]
                    },
                    ...
                }

    Raises
    ------
    ValueError
        When one of the inputs is invalid
    """
    PARSE_TYPES = {"biallelic", "multiallelic"}
    MASK_TYPES = {"hide", "highlight"}
    if min_read_depth < 0 or int(max_AF) > 1 or parse_type not in PARSE_TYPES or str(mask_type) not in MASK_TYPES:
        raise ValueError("Invalid input.")
    if masks != None:
        mask_positions = parse_mask_file(masks)
    #lfv_data = Biallelic() if parse_type == "biallelic" else Multiallelic()
    lfv_data = {}
    data = vcf.Reader(open(filename, 'r'))
    # reference data. { position: {first reference base: [% reads with matching base, read depth}] }
    ref_data = {}
    for record in data:
        # Amount of reads at particular location
        raw_depth = record.INFO['DP']
        pos = record.POS
        variant = str(record.ALT[0])
        ref = str(record.REF[0])
        # Check if DP4 is one of the fields. 
        if 'DP4' in record.INFO:
            # Amount of reads supporting the variant
            var_depth = record.INFO['DP4'][2] + record.INFO['DP4'][3]
            freq = float(var_depth / raw_depth)
        else:
            var_depth = record.INFO["DP"]
            freq = record.INFO["AF"]		    
        #doesn't include masked positions based on user settings
        if masks != None and pos in mask_positions and mask_type == 'hide':
            continue
        # If the variant passes restrictions, store data
        # TODO(ahwang): does record.INFO["AF"] = var_depth / raw_depth when DP4 is not present?
        if _is_valid_variant(min_read_depth, min_AF, max_AF, var_depth, freq):
            #lfv_data.store(pos, var, freq, var_depth)
            if pos not in lfv_data:
                lfv_data[pos] = {variant: [freq, var_depth]}
            else:
                lfv_data[pos].update({variant: [freq, var_depth]})
        if store_reference and pos not in ref_data:
            # Calculate how many reads match the reference at this location
            # aka the number of reads with allele at this position that matches the reference
            if 'DP4' in record.INFO:
                ref_depth = record.INFO['DP4'][0] + record.INFO['DP4'][1]
                ref_freq = ref_depth/raw_depth
            else:
                ref_depth = record.INFO["DP"]
                ref_freq = 1 - record.INFO["AF"]			       
            # If ref allele passes restrictions, store the data
            if _is_valid_variant(min_read_depth, min_AF, max_AF, ref_depth, ref_freq):
                ref_data[pos] = {ref: [ref_freq, ref_depth]}
    # After parsing is complete, make object into a dictionary
    #lfv_data = dict(lfv_data)
    # If we collected reference data, update lfv_data
    if store_reference:
        for pos in ref_data:
            if pos not in lfv_data:
                lfv_data[pos] = ref_data[pos]
            else:
                lfv_data[pos].update(ref_data[pos])
    return lfv_data

def extract_shared_variants(
        donor,
        recip,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type="multiallelic",
        store_reference=True,
        masks=None,
        mask_type="hide",
        store_donor_only_variants=False
):
    """
    Parses a pair of VCF files to find common variants.

    Parameters
    ----------
    donor : str
        Path to a VCF file
    recip : str
        Path to a VCF file
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    store_reference : boolean, optional
        Whether to store data representing reads that match the reference
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    store_donor_only_variants : boolean, optional
        Whether to include variants found only in the donor sample, in the result.

    Returns
    -------
    TransmissionPair
        TransmissionPair instance representing the variants found in the
        provided donor and recipient samples.
    """
    donor_file = donor
    recip_file = recip
    if type(min_AF) is tuple:
        donor_min_AF, recip_min_AF = min_AF
    else:
        donor_min_AF = min_AF
        recip_min_AF = min_AF
    if type(max_AF) is tuple:
        donor_max_AF, recip_max_AF = max_AF
    else:
        donor_max_AF = max_AF
        recip_max_AF = max_AF
    donor_data = extract_variants_from_vcf(
        donor_file,
        min_read_depth=min_read_depth,
        max_AF=donor_max_AF, # formerly .5
        min_AF=donor_min_AF,
        parse_type=parse_type,
        store_reference=store_reference,
        masks=masks,
        mask_type=mask_type)
    recip_data = extract_variants_from_vcf(
        recip_file,
        min_read_depth=min_read_depth,
        max_AF=recip_max_AF, # formerly 1
        min_AF=recip_min_AF,
        parse_type=parse_type,
        store_reference=store_reference,
        masks=masks,
        mask_type=mask_type)
    shared_count = 0
    # Stored as {pos: {var: [donor freq., recip. freq]}} bc Maria had two bb input files 
    # and one required all this info, might change later tho
    bb_data = defaultdict(dict)
    if masks:
        mask_list = parse_mask_file(masks)
    # Iterate through each variant at each position
    for pos in donor_data: 
        if masks and pos in mask_list and mask_type == "hide":
            continue
        for var in donor_data[pos]:
            # Store donor data
            donor_freq = donor_data[pos][var][0]
            donor_depth = donor_data[pos][var][1]
            if not store_donor_only_variants: 
                if pos not in recip_data or var not in recip_data[pos]:
                    continue
            if pos not in bb_data:
                bb_data[pos] = {var: [donor_freq, 0.0, donor_depth, 0.0]}
            else:
                bb_data[pos].update({var: [donor_freq, 0.0, donor_depth, 0.0]})
            # If recipient has same variant at same location, store it
            # TODO(ahwang): Why is .02 and .5 hardcoded??
            # TODO(ahwang): Check that recip_freq passes allele freq requirements
            # TODO(ahwang): Check that read depths for both samples pass the min_read_depth 
            # if pos in recip_data and var in recip_data[pos] and donor_data[pos][var][0] > 0.02 and donor_data[pos][var][0] <= 0.5:
            if pos in recip_data and var in recip_data[pos]:
                recip_freq = recip_data[pos][var][0]
                recip_depth = recip_data[pos][var][1]
                bb_data[pos][var][1] = recip_freq
                bb_data[pos][var][3] = recip_depth
                shared_count += 1
    donor_basename = os.path.basename(donor_file).split(".")[0]
    recip_basename = os.path.basename(recip_file).split(".")[0]
    return TransmissionPair(donor_basename, recip_basename, bb_data, shared_count)

def extract_shared_variants_all_pairs(
        vcf_path,
        masks=None,
        mask_type='hide',
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type='biallelic'
):
    """
    Finds common variants among all possible pairs of VCF files in a directory.

    Parameters
    ----------
    vcf_path : str
        Path to a directory of VCF files
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 10
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    
    Returns
    -------
    List
        List of TransmissionPair instances
    """
    all_pairs = []
    vcf_folder = Path(vcf_path)
    for donor_filename in os.listdir(vcf_folder):
        for recipient_filename in os.listdir(vcf_folder):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                donor_path = os.path.join(vcf_folder, donor_filename)
                recipient_path = os.path.join(vcf_folder, recipient_filename)
                all_pairs.append(
                    extract_shared_variants(
                        donor_path, 
                        recipient_path, 
                        min_read_depth=min_read_depth, 
                        min_AF=min_AF, 
                        max_AF=max_AF, 
                        parse_type=parse_type, 
                        masks=masks, 
                        mask_type=mask_type
                    )
                )
    return all_pairs

def build_majority_consensus(
    vcf_file, 
    reference, 
    masks=None, 
    mask_type='hide',
    parse_type='biallelic'
):
    """
    Constructs consensus sequence containing the majority variants.

    Parameters
    ----------
    vcf_file : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site

    Returns
    -------
    str
        Consensus sequence constructed from the VCF file and reference genome
    """  
    consensus = list(get_seq(reference))
    variants = extract_variants_from_vcf(
        vcf_file, 
        min_AF=.5, # formerly 0. changed to fix https://trello.com/c/9hiDyyYk/212-investigate-why-samples-w-identical-consensus-are-not-aligned-on-the-tree
        max_AF=1, 
        parse_type=parse_type, 
        store_reference=True, 
        masks=None, 
        mask_type='hide'
    )
    highest_variants = {}
    for pos in variants:
        highest_freq = 0
        kept_var = None
        for var in variants[pos]:
            if variants[pos][var][0] > highest_freq:
                highest_freq = variants[pos][var][0]
                kept_var = var
        if kept_var:
            highest_variants[pos] = str(kept_var)
    for pos in highest_variants:
        consensus[pos - 1] = highest_variants[pos]
    return ''.join(consensus)

def build_minor_consensus(
        vcf_file,
        reference,
        min_AF=0,
        max_AF=1, 
        masks=None,
        mask_type='hide',
        parse_type='multiallelic'
):
    """
    Constructs consensus sequence containing variants with the second highest 
    frequency.

    Parameters
    ----------
    vcf_file : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 

        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    
    Returns
    -------
    str
        Consensus sequence containing the second most frequent alleles
    """
    consensus = list(get_seq(reference))
    data = extract_variants_from_vcf(
        vcf_file, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        parse_type=parse_type, 
        store_reference=True, 
        masks=None, 
        mask_type='hide'
    )
    variants = {}
    for pos in data:
        highest_freq = 0
        max_freq = max([data[pos][var][0] for var in data[pos]])
        kept_var = None
        for var in data[pos]:
            freq = data[pos][var][0]
            if freq > highest_freq and freq != max_freq:
                highest_freq = freq
                kept_var = var
        if kept_var:
            variants[pos] = str(kept_var)
    for pos in variants:
        consensus[pos - 1] = variants[pos]
    return ''.join(consensus)

def write_bb_bottleneck_input_file(
        donor,
        recipient,
        parse_type="biallelic",
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        masks=None
):
    """
    Writes input file for BB bottleneck software.

    Parameters
    ----------
    donor : str
        Path to the donor VCF file
    recipient : str
        Path to the recipient VCF file
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    
    Returns
    -------
    str
        Path to .txt file formatted as an input to the BB bottleneck software.
    """
    # Parse data and store outputs
    transm_pair = extract_shared_variants(
        donor, 
        recipient, 
        parse_type=parse_type, 
        min_read_depth=min_read_depth, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        masks=masks,
        store_donor_only_variants=True
    )
    variants = transm_pair.variants
    num_shared = transm_pair.num_shared
    # Remove directory names from file paths
    fname1 = os.path.basename(donor)
    fname2 = os.path.basename(recipient)
    # Remove file extensions
    fname1 = fname1.split(".")[0] 
    fname2 = fname2.split(".")[0]
    # Write data to file in BB_Bottleneck input format
    filename = f"new_{fname1}_{fname2}_thred{num_shared}_complete_nofilter_bbn.txt"
    with open(filename, "w") as f:
        for pos in variants:
            for var in variants[pos]:
                f.write(str(variants[pos][var][0])+'\t'+str(variants[pos][var][1])+'\n')
    return filename

def _estimate_bottleneck_size(
        donor_path,
        recipient_path,
        min_read_depth,
        min_AF,
        max_AF,
        min_shared,
        verbose=False,
        cluster_name=None,
        plot_bool=False,
        masks=None
):
    """
    Estimate the bottleneck size of a given transmission pair.

    Parameters
    ----------
    donor_path : str
        Path to a VCF file
    recipient_path : str
        Path to a VCF file
    min_read_depth : int
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int
        Minimum number of variants the pair must share
    verbose : boolean, optional
        Whether to print the command(s) being executed
    cluster_name : str, optional
        Name of the cluster that the samples belong to, if known.
    plot_bool : bool, optional
        Whether to produce the log likelihood vs bottleneck size plot
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    
    Returns
    -------
    List or None
        [
            cluster_name, 
            bottleneck_size, 
            left bound, 
            right bound]
        ]
    """
    transm_pair = extract_shared_variants(
        donor_path,
        recipient_path,
        min_read_depth=min_read_depth,
        min_AF=min_AF,
        max_AF=max_AF,
        masks=masks,
        store_donor_only_variants=True
    )
    num_shared = transm_pair.num_shared
    if num_shared < min_shared:
        return None
    filename = write_bb_bottleneck_input_file(donor_path,
        recipient_path,
        min_read_depth=min_read_depth,
        min_AF=min_AF,
        max_AF=max_AF,
        masks=masks
    )
    with resources.path('trakit.scripts', 'Bottleneck_size_estimation_approx.r') as bottleneck_script:
        bb_command = f"Rscript {bottleneck_script} --file {filename} --var_calling_threshold 0.0001 --Nb_min 1 --Nb_max 200 --confidence_level .95"
        # --plot_bool FALSE does not actually prevent the plot from being produced
        if plot_bool:
            bb_command += f" --plot_bool TRUE"
        if verbose:
            print(bb_command)
        proc = subprocess.Popen(bb_command, shell=True, stdout=subprocess.PIPE)
        outs, _ = proc.communicate() # Read data from stdout 
        outs = outs.decode()
        output_lst = outs.split("\n[1]")
        output_lst = [s.lstrip(" \r").rstrip("\n") for s in output_lst]
        if output_lst[0] == '[1] "Bottleneck size"':
            bottleneck_size = int(output_lst[1])
            left_bound_interval = int(output_lst[3])
            right_bound_interval = int(output_lst[5])
            cluster_name = cluster_name
            return [cluster_name, bottleneck_size, left_bound_interval, right_bound_interval]
        else:
            return None
    return None

def plot_bottleneck_in_clusters(
        vcf_directory,
        metadata_path,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        min_shared=1,
        save=True,
        output_folder=None,
        verbose=False,
        masks=None,
        legend_loc="upper left"
):
    """
    Plot bottleneck size of sample pairs belonging to the same cluster.
    see all_pairs for parameter definitions.

    Parameters
    ----------
    vcf_directory : str
        Path to a directory of VCF files
    metadata_path : str
        Path to a TSV file labeling samples
        with metadata. If metadata is provided, the bottleneck size will only be
        estimated for pairs in the same cluster as defined in the metadata.
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int, optional
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the visualization as a PNG file
    output_folder : str, optional
        Path to folder to store the generated visualization PNG file in
    verbose : boolean, optional
        Whether to print the command(s) being executed
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    legend_loc : str or pair of floats, optional
        Location of the legend. See matplotlib.pyplot.legend documentation for 
        acceptable values (ex. 'upper left', 'upper right')
    Returns
    -------
    List 
        Maps pair ID (integer) to the corresponding pair of samples.
    """
    metadata_cluster_assignments = {}
    metadata_clusters = defaultdict(list)
    file = open(metadata_path)
    read_tsv = csv.reader(file, delimiter="\t")
    next(read_tsv) # Assume column names are present
    for line in read_tsv:
        basename = line[0].strip()
        group_id = line[1]
        metadata_cluster_assignments[basename]=group_id
        metadata_clusters[group_id].append(basename)
    # Use dictionaries to group entries by cluster.
    bottleneck_sizes, left_bound_intervals, right_bound_intervals, pair_nums = defaultdict(list), defaultdict(list), defaultdict(list), defaultdict(list)
    pair_num = 1
    sample_pairs = [set()]
    all_pairs_input = []
    for donor_filename in os.listdir(vcf_directory):
        for recipient_filename in os.listdir(vcf_directory):
            donor_basename = donor_filename.split(".")[0]
            recipient_basename = recipient_filename.split(".")[0]
            if donor_filename != recipient_filename and \
                donor_basename in metadata_cluster_assignments and \
                recipient_basename in metadata_cluster_assignments and \
                metadata_cluster_assignments[donor_basename] == metadata_cluster_assignments[recipient_basename]:
                donor_path = os.path.join(vcf_directory, donor_filename)
                recipient_path = os.path.join(vcf_directory, recipient_filename)
                # Estimate the transmission bottleneck size.
                bottleneck_estimate = _estimate_bottleneck_size(
                    donor_path, 
                    recipient_path, 
                    min_read_depth, 
                    min_AF, 
                    max_AF, 
                    min_shared, 
                    verbose=verbose, 
                    cluster_name=metadata_cluster_assignments[recipient_basename],
                    plot_bool=False,
                    masks=masks
                )
                sample_names = set([donor_basename, recipient_basename])
                if bottleneck_estimate:
                    all_pairs_input.append(bottleneck_estimate)
                    sample_pairs.append(sample_names)
    all_pairs_input.sort(key=lambda x:x[0])
    pair_num = 1
    for read in all_pairs_input:
        cluster_name = read[0]
        bottleneck_sizes[cluster_name].append(read[1])
        left_bound_intervals[cluster_name].append(read[2])
        right_bound_intervals[cluster_name].append(read[3])
        pair_nums[cluster_name].append(pair_num)
        pair_num += 1
    plt.figure(figsize=(10,8))
    N = len(metadata_clusters)
    cmap = plt.cm.get_cmap("gist_rainbow", N+1)
    i = 0
    # The pair_nums mapping values are in chronological order.
    for group_id in pair_nums.keys():
        color = cmap(i)
        plt.errorbar(pair_nums[group_id],bottleneck_sizes[group_id], np.asarray([left_bound_intervals[group_id],right_bound_intervals[group_id]]), ls = "None", color = color, label=group_id)
        plt.scatter(pair_nums[group_id],bottleneck_sizes[group_id],color=color, s=32,marker="o")
        i += 1
    xtick_fontsize = 24 if pair_num < 11 else (18 if pair_num < 21 else 12)
    ytick_fontsize = 24 if pair_num < 11 else 20
    plt.xlabel('Pair ID', fontweight='bold', fontsize=26)
    plt.ylabel('Bottleneck Size', fontweight='bold', fontsize=26)
    plt.legend(title="Cluster", loc=legend_loc)
    plt.tight_layout()
    plt.yticks(fontsize = ytick_fontsize)
    plt.xticks(range(1, pair_num), range(1, pair_num), fontsize = xtick_fontsize)
    plt.show()
    if save:
        if output_folder == None:
            path = os.path.join(vcf_directory, 'bottleneck')
        else:
            path = os.path.join(output_folder, 'bottleneck')
        filename = 'bottleneck_chart.png'
        Path(path).mkdir(parents=True, exist_ok=True)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')
    return sample_pairs
    

def plot_bottleneck_all_pairs(
        vcf_directory,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        min_shared=1,
        save=True,
        output_folder=None,
        verbose=False,
        masks=None
):
    """
    Plot bottleneck size of all pairs of samples whose number of shared variants
    exceed a certain threshold

    Parameters
    ----------
    vcf_directory : str
        Path to a directory of VCF files
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int, optional
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the visualization as a PNG file
    output_folder : str, optional
        Path to folder to store the generated bottleneck size chart PNG file in
    verbose : boolean, optional
        Whether to print the command(s) being executed
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
   
    Returns
    -------
        list 
            Maps pair ID (list index, starting at 1) to the corresponding pair of samples.
    """
    bottleneck_sizes, left_bound_intervals, right_bound_intervals, pair_nums = [], [], [], []
    pair_num = 1
    sample_pairs = [set()]
    all_pairs_input = []
    for donor_filename in os.listdir(vcf_directory):
        for recipient_filename in os.listdir(vcf_directory):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                donor_path = os.path.join(vcf_directory, donor_filename)
                recipient_path = os.path.join(vcf_directory, recipient_filename)
                # Estimate the transmission bottleneck size.
                bottleneck_estimate = _estimate_bottleneck_size(
                    donor_path, 
                    recipient_path, 
                    min_read_depth, 
                    min_AF, 
                    max_AF, 
                    min_shared, 
                    verbose=verbose,
                    plot_bool=False,
                    masks=masks
                )
                if bottleneck_estimate:
                    all_pairs_input.append(bottleneck_estimate)
                    donor = donor_filename.split(".")[0] 
                    recipient = recipient_filename.split(".")[0]
                    sample_names = set([donor, recipient])
                    sample_pairs.append(sample_names)	
    # Sort pairs by bottleneck size.
    all_pairs_input.sort(key=lambda x:int(x[1]))
    pair_num = 1
    for read in all_pairs_input:
        bottleneck_sizes.append(read[1])
        left_bound_intervals.append(read[2])
        right_bound_intervals.append(read[3])
        pair_nums.append(pair_num)
        pair_num += 1  
    plt.figure(figsize=(10,8))
    plt.errorbar(pair_nums,bottleneck_sizes, np.asarray([left_bound_intervals,right_bound_intervals]), ls = "None", color = "gray")
    plt.scatter(pair_nums,bottleneck_sizes,c='blue', s=32,marker="o")
    plt.xlabel('Pair ID', fontweight='bold',fontsize=26)
    plt.ylabel('Bottleneck Size', fontweight='bold',fontsize=26)
    plt.tight_layout()
    plt.yticks(fontsize = 24)
    plt.xticks(range(1, pair_num), range(1, pair_num), fontsize = 24)
    plt.show()
    if save:
        if output_folder == None:
            path = os.path.join(vcf_directory, 'bottleneck')
        else:
            path = os.path.join(output_folder, 'bottleneck')
        filename = 'bottleneck_chart.png'
        Path(path).mkdir(parents=True, exist_ok=True)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')
    return sample_pairs

# TODO: Clarify abbreviations or names that aren't intuitive. 
# For example, sv = shared variants. high_shared = threshold.
def count_shared_variants(transm_pairs_list, mask_pairs_list=None, high_shared=6):
    """
    Counts the number shared variants in all possible pairs.

    Parameters
    ----------
    transm_pairs_list : list
        list of TransmissionPair instances
    mask_pairs_list : list, optional
        list of TransmissionPair instances after masking
    high_shared : int, optional
        minimum number of variants a pair of samples must share to be "important"

    Returns
    -------
    dict
        If mask_pairs_list is not provided:

                {
                    num shared variants: num pairs with that num shared variants
                }

        If mask_pairs_list is provided:

                {   
                    num shared variants: [
                        num pairs with that num shared variants,
                        num pairs with that num shared variants after masking
                    ]
                }
    List
        List of pairs that meet the threshold amount of shared variants
    """
    def masked_default_value():
        return [0,0]
    if mask_pairs_list:
        sv_count = defaultdict(masked_default_value)
    else:
        sv_count = defaultdict(int) # tracks number of pairs with a given count.
    important_pairs = []
    #Unmasked list
    for transm_pair in transm_pairs_list:
        # mapping of site => variant => frequency/depth information
        # The old logic assumed that a variant is shared whenever recip freq != 0.
        # Thus, it always made a pass through TransmissionPair.variants.
        # However, we can just rely on TransmissionPair.num_shared attribute.
        pair_sv = transm_pair.num_shared
        if not mask_pairs_list:
            sv_count[pair_sv] += 1
        else: 
            sv_count[pair_sv][0] += 1
        if pair_sv > high_shared: # TODO: high_shared = threshold
            important_pairs.append((transm_pair.donor_basename, transm_pair.recipient_basename))
    # At this point, we can guarantee that every value in sv_count mapping is of the same format. They're all either a number or they're a list of 2 numbers.
    #Masked list
    if mask_pairs_list:
        # We can guarantee that every value in sv_count mapping is a list of 2 numbers.
        for transm_pair in mask_pairs_list:
            mask_pair_sv = transm_pair.num_shared
            if mask_pair_sv in sv_count:
                sv_count[mask_pair_sv][1] += 1
            if mask_pair_sv > high_shared:
                important_pairs.append((transm_pair.donor_basename, transm_pair.recipient_basename))
    return sv_count, important_pairs

def position_counter(transm_pairs):
    """
    Counts number of pairs that have variants at each position

    Parameters
    ---------- 
    transm_pairs : list
        A list of TransmissionPair instances
    
    Returns
    -------
    dict
        {
            position: number of pairs with a variant at that position
        }
    """
    position_count = defaultdict(int)
    for transm_pair in transm_pairs:
        variants = transm_pair.variants
        for pos, var_mapping in variants.items():
            for var in var_mapping:
                if var_mapping[var][1] != 0:
                    position_count[pos] += 1
    return position_count

def plot_allele_frequencies_all_pairs(
        vcf_path,
        plot_type='unweighted',
        min_read_depth=10,
        min_AF=MIN_AF,
        max_AF=1,
        parse_type='biallelic',
        min_shared=4,
        save=True,
        output_path=None,
        masks=None,
        mask_type='hide'
):
    """
    Generates an allele frequency bar plot for every possible pair of samples.

    Parameters
    ----------
    vcf_path : str
        Path to a directory of VCF files
    plot_type : {'unweighted', 'weighted'}, optional
        Whether to weight the thickness of bars by read depth
    min_read_depth : int, optional
        Minimum number of reads required to support a variant
    min_AF : float 
        Minimum allele frequency to plot a variant
    max_AF : float 
        Maximum allele frequency to plot a variant
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    min_shared : int
        Minimum number of variants the pair must share
    save : boolean, optional
        Whether to save the bar plots as PNG files
    output_folder : str, optional
        Path to folder to store the generated PNG files in
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    """
    vcf_folder = Path(vcf_path)
    #shown = 0
    for donor_filename in os.listdir(vcf_folder):
        for recipient_filename in os.listdir(vcf_folder):
            if donor_filename != recipient_filename and _is_vcf_file(donor_filename) and _is_vcf_file(recipient_filename):
                # Only create a bar plot if the donor-recipient pair shares a minimum number of variants
                donor_path = os.path.join(vcf_folder, donor_filename)
                recipient_path = os.path.join(vcf_folder, recipient_filename)
                transm_pair = extract_shared_variants(donor_path, recipient_path)
                if transm_pair.num_shared > min_shared:
                    plot_allele_frequencies(
                        donor_path, 
                        recipient_path, 
                        min_read_depth=min_read_depth, 
                        min_AF=min_AF,
                        max_AF=max_AF, 
                        save=save, 
                        output_folder=output_path, 
                        parse_type=parse_type, 
                        store_reference=True,  # TODO(ahwang): Make this a parameter?
                        plot_type=plot_type, 
                        masks=masks, 
                        mask_type=mask_type)
                    # TODO: What is the reasoning behind only showing the first 5? Was this just for testing?
                    # TODO: This check is ineffective because make_bar_plot displays the plot upon creation.
                    # if shown <= 5:
                    # 	plt.show()
                    # 	shown += 1
                

def plot_allele_frequencies(
        donor_path,
        recipient_path,
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        save=True,
        output_folder=None,
        parse_type="biallelic",
        store_reference=True,
        plot_type='unweighted',
        masks=None,
        mask_type='hide'
):
    """
    Generate an allele frequency bar plot for a pair of samples

    Parameters
    ----------
    donor_path : str
        Path to a VCF file
    recipient_path : str
        Path to a VCF file
    min_read_depth : int
        Minimum read depth to plot a variant
    min_AF : float 
        Minimum allele frequency to plot a variant
    max_AF : float 
        Maximum allele frequency to plot a variant
    save : boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder: str, optional
        Path to the folder in which the allele frequency chart will be saved. 
    parse_type: str, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    store_reference : boolean, optional
        Whether to include reference alleles in the plot
    plot_type: {'unweighted', 'weighted'}, optional
        Whether the thickness of bars should be weighted by read depth
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is unused)
        The desired action to do with the sites listed in the masks file.
    """
    donor_basename = os.path.basename(donor_path).split(".")[0]
    recipient_basename = os.path.basename(recipient_path).split(".")[0]
    PARSE_TYPES = {"biallelic", "multiallelic"}
    PLOT_TYPES = {"unweighted", "weighted"}
    MASK_TYPES = {"hide", "highlight"}
    if parse_type not in PARSE_TYPES or plot_type not in PLOT_TYPES or mask_type not in MASK_TYPES:
        raise ValueError("Invalid input.")
    transm_pair = extract_shared_variants(
        donor_path, 
        recipient_path, 
        min_read_depth=min_read_depth, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        parse_type=parse_type, 
        store_reference=store_reference, 
        masks=masks, 
        mask_type=mask_type)
    variants = transm_pair.variants
    if len(variants) == 0:
        return None
    positions, donor_freqs, recipient_freqs, donor_depths, recipient_depths = [], [], [], [], []
    # Create plot labels
    donor_label = str('Donor: ' + donor_basename)
    recipient_label = str('Recipient: ' + recipient_basename)
    max_read = 0
    min_read = float("inf")
    for pos in sorted(variants):
        # TODO: (nit) change "nuc" to "var" for consistency
        for nuc in variants[pos]:
            positions.append((pos, nuc))
            donor_freqs.append(round(variants[pos][nuc][0], 4))
            recipient_freqs.append(round(variants[pos][nuc][1], 4))
            donor_depths.append((variants[pos][nuc][2]))
            recipient_depths.append((variants[pos][nuc][3]))
    # TODO: only compute these values and textstr if the plot_type is weighted
    max_read = max(x for x in donor_depths + recipient_depths if x != 0)
    min_read = max(0, min(x for x in donor_depths + recipient_depths if x != 0))       
    width_divider = max_read*3
    donor_depths = [x/width_divider for x in donor_depths]
    recipient_depths = [x/width_divider for x in recipient_depths]
    textstr = str("Allele Frequencies   Max Depth: " + str(int(max_read)) + " Min Depth: " + str(int(min_read)))
    # the label locations
    x = np.arange(len(positions)) 
    # the width of the bars
    width = 0.35  
    fig, ax = plt.subplots()
    if plot_type == 'unweighted':
        ax.bar(x - width/2, donor_freqs,  color='#7f6d5f', width=width,
            edgecolor='white', label=donor_label)
        ax.bar(x + width/2, recipient_freqs, color='#557f2d', width=width,
            edgecolor='white', label=recipient_label)
    if plot_type == 'weighted':
        ax.bar(x - width/2, donor_freqs,  color='#7f6d5f', width=donor_depths,
            edgecolor='white', label=donor_label)
        ax.bar(x + width/2, recipient_freqs, color='#557f2d', width=recipient_depths,
            edgecolor='white', label=recipient_label) 
    # Add some text for labels, title and custom x-axis tick labels, etc.
    if plot_type == 'weighted':
        ax.set_ylabel(textstr)
    else:
        ax.set_ylabel("Allele Frequencies")
    # ax.set_xticks(x)
    # ax.set_xticklabels(positions)
    # ax.tick_params(axis = 'x', rotation = 50)
    ax.legend()
    maxn = len(positions)
    fig.tight_layout() 
    plt.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(8))
    plt.xlim(-0.5,maxn-0.5)
    plt.xticks(ticks=x, labels=positions, rotation=50)
    plt.tight_layout()
    #save = os.path.join(output_folder, filename)
    #plt.savefig(save, dpi=300, bbox_inches='tight')
    # TODO: Uncommenting this does not prevent all the plots from showing.
    #plt.show()
    # Download the image only if the save flag is turned on
    if save:
        folder = str(plot_type + "_bar_plots")
        if output_folder != None:
            path = os.path.join(output_folder, folder)
        else:
            path = folder
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = '%s_%s_allele_freq.png'%(donor_basename, recipient_basename)
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')

def plot_shared_variant_counts(transm_pairs_list, mask_pairs_list, high_shared=6, save=True, output_folder=None):
    """
    Create chart showing number of sample pairs sharing a certain number of 
    variants when genome is both masked and unmasked. Use the outputs of
    count_shared_variants() as inputs.
    
    Parameters
    ----------
    transm_pairs_list : list
        list of TransmissionPair instances
    mask_pairs_list : list, optional
        list of TransmissionPair instances after masking
    high_shared : int, optional
        minimum number of variants a pair of samples must share to be "important"
    save: boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder : str, optional
        Path to the folder in which the masked shared variants chart will be saved. 
    """
    # Counts the number shared variants in all possible pairs of samples
    sv_count, important_pairs = count_shared_variants(
        transm_pairs_list, 
        mask_pairs_list=mask_pairs_list, 
        high_shared=high_shared)
    shared_variants = sorted(sv_count.items())
    sv, masked_pairs, complete_pairs, cell_text, columns, masked, complete= [], [], [], [], [], [], []
    for num in shared_variants:
        sv.append(num[0])
        complete_pairs.append(num[1][0])
        complete.append(num[1][0])
        masked_pairs.append(num[1][1])
        masked.append(num[1][1])
    cell_text.append(masked)
    cell_text.append(complete)
    columns = tuple(sv)
    rows = ['Masked Genome', 'Complete Genome']
    colors = ['blue', 'red']
    masked_label = "Masked Genome"
    complete_label = "Complete Genome"
    x = np.arange(len(sv))  # the label locations
    width = 0.35  # the width of the bars
    plt.figure(figsize=(10,6))
    plt.bar(x - width/2, masked_pairs,  color='blue', width=width,
        edgecolor='white', label=masked_label)
    plt.bar(x + width/2, complete_pairs, color='red', width=width,
        edgecolor='white', label=complete_label)
    plt.table(cellText=cell_text, rowLabels=rows, rowColours=colors, colLabels=columns)
    # Add some text for labels, title and custom x-axis tick labels, etc.
    maxn = len(sv)
    plt.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(12))
    plt.xlim(-0.5,maxn-0.5)
    plt.xticks([])
    plt.ylabel("Number of Pairs")
    plt.xlabel("Number of Shared Variants", labelpad=50)
    # Download the image only if the save flag is turned on
    if save:
        folder = "msv"
        if output_folder != None:
            path = os.path.join(output_folder, folder)
        else:
            path = folder
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = 'msv_chart'
        save = os.path.join(path, filename)
        plt.savefig(save, dpi=300, bbox_inches='tight')


# TODO: Should a mask file always be supplied? What if the user just wants to view top 20 
# most common positions, unmasked?
def plot_top_shared_variant_sites(
        transmission_pairs,
        mask_file,
        input_folder,
        save=True,
        output_folder=None,
        shown_variants=20
):
    """
    Creates chart showing top positions that have the most pairs with a variant at that position

    Parameters
    ----------
    transmission_pairs : List
        List of TransmissionPair instances
    mask_file : str
        Path to a txt file containing comma-separated genomic sites to mask
    input_folder : str
        Path to a folder containing VCFs
    save : boolean, optional
        Whether to save this visualization as a PNG file.
    output_folder: str, optional
        Path to the folder in which the chart will be saved. 
    shown_variants : int, optional
        Number of most common variants to plot
    """
    def barplot_properties():
        return {
            'xloc': [],
            'site': [],
            'frequency': [],
        }
    position_count = position_counter(transmission_pairs)
    mask_positions = parse_mask_file(mask_file)
    sorted_pos = sorted(position_count.items(), key=lambda x: x[1], reverse=True)
    barplot_data  = defaultdict(barplot_properties)
    sorted_pos = sorted_pos[0:shown_variants]
    position_data = [pos[0] for pos in sorted_pos]
    x = np.arange(len(sorted_pos))  # the label locations
    for idx, pos in zip(x, sorted_pos):
        if pos[0] in mask_positions:
            category = 'masked'
        else:
            category = 'unmasked'
        barplot_data[category]['xloc'].append(idx)
        barplot_data[category]['site'].append(pos[0])
        barplot_data[category]['frequency'].append(pos[1]) # number of pairs
    width = 0.35  # the width of the bars
    fig, ax = plt.subplots()
    masked_label = "Masked Site"
    complete_label = "Unmasked Site" 
    ax.bar(
        barplot_data['masked']['xloc'],
        barplot_data['masked']['frequency'],
        color='red',
        width=width,
        label=masked_label
    )  
    ax.bar(
        barplot_data['unmasked']['xloc'],
        barplot_data['unmasked']['frequency'], 
        color='blue',
        width=width,
        label=complete_label
    )  
    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Number of Pairs')
    ax.set_xlabel("Variant Position")
    ax.set_xticks(x)
    ax.set_xticklabels(position_data)
    ax.tick_params(axis = 'x', rotation = 50)
    fig.tight_layout()  
    # Using patches is necessary because the legend colors might not be
    # correct if the plot doesn't include either a masked or unmasked site.
    red_patch = mpatches.Patch(color='red', label=masked_label)
    blue_patch = mpatches.Patch(color='blue', label=complete_label)
    plt.legend(loc='lower left', bbox_to_anchor= (.15, 1.01), ncol=2, borderaxespad=0, frameon=False, fontsize=(12), handles=[red_patch, blue_patch])
    maxn = len(position_data)
    plt.xlim(-0.5,maxn-0.5)
    plt.tight_layout() 
    plt.show()
    if save:  
        if output_folder != None:
            path = os.path.join(output_folder, 'positions')
        else:
            path = os.path.join(input_folder, 'positions')
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = 'positions_chart'
        save = os.path.join(path, filename)
        fig.savefig(save, dpi=300, bbox_inches='tight')

THREADS = 1

class FastaAligner:
    """
    Class for aligning FASTA files.

    This class is meant for handling FASTA files with one sequence per file, and will raise
    an error if given a multi-FASTA file.
    """
    def __init__(self, fasta_dir):
        """
        Parameters
        ----------
        fasta_dir : str
            Path to a directory of FASTA files
        """
        if os.path.isdir(fasta_dir):
            self.dir = fasta_dir
        else:
            raise ValueError("The specified path should be a directory of fasta files.")

    def align(self, reference, output_dir='', threads=THREADS, verbose=False):
        """
        Aligns all FASTA files in a directory.

        Parameters
        ----------
        reference : str
            Path to reference genome FASTA file
        output_dir : str, optional
            Path to directory to store Parsnp and HarvestTools outputs in
        threads : int, optional
            Number of threads to use when running Parsnp
        verbose : boolean, optional
            Whether to print the command(s) being executed
        """
        if not os.path.exists(reference):
            raise FileNotFoundError(f"{reference}")
        Path(output_dir).mkdir(parents=True, exist_ok=True)       
        output = os.path.join(os.getcwd(), output_dir)
        # -d: (d)irectory containing genomes/contigs/scaffolds
        # -r: (r)eference genome (set to ! to pick random one from genome dir)
        # -o: output directory? default [./P_CURRDATE_CURRTIME]
        # -p: number of threads to use? (default= 1)
        cmnd = f"parsnp -d {self.dir} -r {reference} -o {output} -p {threads}"
        if verbose:
            print("cmnd: " + cmnd)
        subprocess.call(cmnd.split())
        path2xmfa = os.path.join(output, 'parsnp.xmfa')
        # -x: xmfa alignment file
        # -M: <multi-fasta alignment output (concatenated LCBs)>
        cmnd2 = f"harvesttools -x {path2xmfa} -M " + os.path.join(output_dir,'parsnp.mfa')
        if verbose:
            print("cmnd: " + cmnd2)
        subprocess.call(cmnd2.split())

LINE_WRAP = 80 # Max. length of each line in fasta file 

def vcf2fasta(
        vcf_path, 
        reference, 
        output_dir="", 
        line_length=LINE_WRAP, 
        min_AF=0,
        max_AF=1,
        parse_type='biallelic',
        masks=None,
        mask_type='hide',
        consensus_type='majority'
):
    """
    Writes a FASTA file containing a constructed consensus sequence.

    Parameters
    ----------
    vcf_path : str
        Path to a VCF file
    reference : str
        Path to a FASTA file representing the reference genome
    output_dir : str, optional
        Path to a directory to save the generated FASTA file in
    line_length : int, optional
        Maximum number of characters per line allowed in the FASTA file
    min_AF : float, optional
        Minimum allele frequency to validate a variant
    max_AF : float, optional
        Maximum allele frequency to validate a variant
    parse_type : {'biallelic', 'multiallelic'}, optional (unused)
        Whether to handle up to 2 or multiple variants per site
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    mask_type : {'hide', 'highlight'}, optional  ('highlight' is not used)
        The desired action to do with the sites listed in the masks file.
    consensus_type : {'majority', 'minority'}, optional
        Whether to construct the consensus sequence containing majority or
        minority variants
    
    Raises
    ------
    ValueError
        When the consensus_type input is invalid
    """
    # If user specifies output directory
    if output_dir:
        # Make directory if it doesn't exist
        Path(output_dir).mkdir(parents=True, exist_ok=True)
    basename = os.path.basename(vcf_path).split('.')[0]
    path = os.path.join(output_dir, basename + '.consensus.fasta')
    if consensus_type == 'majority':
        seq = build_majority_consensus(
            vcf_path, 
            reference, 
            masks=None, 
            mask_type='hide',
            parse_type=parse_type
        )
    elif consensus_type == 'minor':
        seq = build_minor_consensus(
            vcf_path, 
            reference, 
            min_AF=0, 
            max_AF=1, 
            masks=None, 
            mask_type='hide',
            parse_type=parse_type
        )
    else:
        raise ValueError(f'Unexpected consensus_type: {consensus_type}.')
    with open(path, 'w') as f:
        f.write(f'>{basename}\n')
        for i in range(0, len(seq), line_length):
            f.write(seq[i: i+line_length] + '\n')

def get_seq(sequence):
    """
    Returns string representation of a genome sequence given a FASTA file.
    Assumes only one sequence in the file
    """    
    consensus = str()
    with open(sequence, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
        for line in lines[1:]:
            consensus += line
            if line and line[0] == '>':
                raise ValueError('File should only include one sequence.')
    return consensus

class FastaRecord:
    """
    Simple class for storing and accessing data from a FASTA record.

    Attributes
    ----------
    name : str
        Name of the sequence
    sequence : str
        The sequence
    """
    def __init__(self, name, sequence):
        if not isinstance(name, str):
            raise TypeError('Name should be a string.')
        if not isinstance(sequence, str):
            raise TypeError('Sequence should be a string.')
        self.name = name
        self.seq = sequence
    
    def set_id(self, identifier):
        """
        identifier : str, int, or float
            ID of this record
        """
        if not isinstance(identifier, (str, int, float)):
            raise TypeError('ID should be a string, integer, or float.')
        self.id = identifier

class MultiFastaParser:
    """
    Class for storing and accessing data from a multi-FASTA file.

    Attributes
    ----------
    multifasta : str
        Path to a multi-FASTA file
    """
    def __init__(self, multifasta):
        if os.path.isfile(multifasta):
            self.fasta = multifasta
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), multifasta) 
        self.records = []
        # Populates self.records with FastaRecord objects
        with open(self.fasta, 'r') as f:
            lines = [line.strip() for line in f.readlines()]
            length = len(lines)
            for i in range(0, length - 1):
                if lines[i][0] == '>':
                    j = 1
                    while i+j < length:
                        if lines[i+j][0] == '>':
                            break
                        j += 1
                    consensus = ''.join(line for line in lines[i+1:i+j])
                    name = lines[i][1:]
                    self.records.append(FastaRecord(name, consensus))

    def get_groups(self):
        """
        Returns mapping of sequence to names of records with that sequence.
        """
        groups = dict()
        for record in self.records:
            if record.seq in groups:
                groups[record.seq].add(record.name)
            else:
                groups[record.seq] = {record.name}
        return list(groups.values())

    def infer_phylogeny(
            self, 
            output_dir='', 
            label='tree', 
            threads=THREADS, 
            custom_cmd='', 
            verbose=False
    ):
        """
        Runs RAxML on the multi-FASTA file and stores output in output_dir.

        Parameters
        ----------
        output_dir : str, optional
            Path to a directory to save the generated tree file(s) in
        label : str, optional
            Specifies the name of the output file.
        threads : int, optional TODO(ahwang): threads or random number seed?
            Number of threads to use when running RAxML
        custom_cmd : str, optional
            Command to run inplace of the default RAxML command
        verbose : boolean, optional
            Whether to print the command being executed
        """
        # If directory already exists, delete all files with same label
        if output_dir:
            if os.path.exists(output_dir):
                for fname in os.listdir(output_dir):
                    if fname.endswith(f".{label}"):
                        os.remove(os.path.join(output_dir, fname))
            else:
                Path(output_dir).mkdir(parents=True, exist_ok=True)
        # If user specifies a command, then run that instead
        if custom_cmd:
            cmnd = custom_cmd
        # Otherwise, run RaxML with default arguments
        else:
            cwd = os.getcwd()
            path = os.path.join(cwd, self.fasta)
            # -s: Specify the name of the alignment data file in PHYLIP format
            # -w: FULL (!) path to the directory into which RAxML shall write its output files (DEFAULT: current directory)
            # -n: Specifies the name of the output file.
            # -m: Model of Binary (Morphological), Nucleotide, Multi-State, or Amino Acid Substitution 
            # -p: Specify a random number seed for the parsimony inferences. This allows you to reproduce your results and will help me debug the program.
            raxmlHPC_path = _add_executable_path('raxmlHPC')
            cmnd = f"{raxmlHPC_path} -s {path} -w {os.path.join(cwd, output_dir)} \
                -n {label} -m GTRCAT -p 12345" #  -p {threads}
            if verbose:
                print(cmnd)
        subprocess.call(cmnd.split())
        # Print out when finished
        if verbose: 
            msg = f'Ran RAxML on {self.fasta} and stored files in directory: {output_dir}' + '.'
            print(msg)

class Variant:
    """
    Representation of one single nucleotide polymorphism.

    Attributes
    ----------
    pos: int
        reference position
    ref : str
        reference allele
    alt : str
        alternate allele
    freq : float
        alternate allele frequency
    """
    def __init__(self, pos, ref, alt, freq):
        self.pos = pos
        self.ref = ref
        self.alt = alt
        self.freq = freq
    
    def str2(self):
        return "%s%s%s (%s)"%(self.ref, self.pos, self.alt, self.freq)
    
    def __str__(self):
        return "%s%s%s"%(self.ref, self.pos, self.alt)
    
    def __eq__(self, other):
        return self.pos == other.pos and self.ref == other.ref and self.alt == other.alt

    def __lt__(self, other):
        return self.pos < other.pos or (self.pos == other.pos and self.alt < other.alt)
    
    def __hash__(self):
        return hash((self.pos, self.ref, self.alt))

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

class PhyloTree:
    """
    Class for visualizing phylogenies using toytree

    Attributes
    ----------
    output_directory : str
        Path to folder to place phylogenetic tree related files
    reference_fasta : str
        Path to reference genome FASTA file
    vcf_directory : str
        Path to a directory containing VCF files
    metadata_path : str, optional
        Path to a TSV file labeling samples with metadata
    has_column_names: boolean, optional
        Whether the first row of the TSV file contains the column names
    """
    def __init__(self, output_directory, reference_fasta, vcf_directory, metadata_path=None, has_column_names=False):
        self.output_directory = output_directory
        self.reference_fasta_path = reference_fasta
        self.reference_fasta_base = os.path.basename(reference_fasta)
        self.vcf_directory = vcf_directory
        self.metadata_path = metadata_path
        self.metadata_cluster_assignments = {} # Sample name => Cluster name
        self.metadata_clusters = defaultdict(list) # Cluster name => [Sample names]
        if self.metadata_path:
            self.make_metadata_clusters(metadata_path, has_column_names)
        # Reference genome DNA sequence
        self.reference = ""
        file1 = open(self.reference_fasta_path)
        next(file1)
        for line in file1:
            self.reference+=line.rstrip()
        # "consensus/sequences" folder for storing the consensus sequence FASTA of each sample
        Path(os.path.join(self.output_directory, "consensus", "sequences")).mkdir(parents=True, exist_ok=True)
        self.file_basenames = [] # List of consensus fasta file base names without the extensions
        self.consensus_dna_dict = {} # consensus fasta file name => consensus DNA sequence. 
        # This includes the consensus sequences for each sample AND the reference.
        self.consensus_dna_dict[self.reference_fasta_base.split(".")[0]] = self.reference
        # List of SNPs observed from all samples
        self.total_snps = set()
        # Map each sample to a list of its SNPs
        self.snps = defaultdict(list)
        self.file_basenames.append(self.reference_fasta_base.split(".")[0])
        for file in os.listdir(self.vcf_directory):
            if file.endswith(".vcf"):
                file_dict = extract_variants_from_vcf(
                    os.path.join(self.vcf_directory, file))
                # Use the reference genome as the original sequence
                dna_seq = str(self.reference)
                for pos, variants in file_dict.items():
                    highest_freq = 0
                    kept_var = None
                    for variant, info in variants.items():
                        # AF = allele frequency
                        freq = info[0]
                        reference_char = self.reference[int(pos)-1] 
                        # majority allele / SNP
                        if freq > 0.5 and variant != reference_char:
                        #if freq > highest_freq and variant != reference_char:
                            highest_freq = freq
                            kept_var = variant
                    if kept_var:
                        dna_seq = dna_seq[:int(pos)-1]+kept_var+dna_seq[int(pos):]
                        basename = file.split(".")[0]
                        snp = Variant(int(pos), reference_char, kept_var, highest_freq)
                        self.snps[basename].append(snp)
                        self.total_snps.add(snp)  
                # Do not rename the reference FASTA file.
                fasta_filename = "%s.consensus.fasta"%(file.split(".")[0])		
                fasta_basename = fasta_filename.split(".")[0]
                # handle = open(os.path.join(self.output_directory, "consensus", "sequences", fasta_filename), "w") 
                # handle.write(">%s\n%s\n"%(file.split(".")[0],dna_seq))
                self.file_basenames.append(fasta_basename)
                self.consensus_dna_dict[fasta_basename] = dna_seq
        # Sort SNPs by genomic position
        self.total_snps = sorted(list(self.total_snps), key=lambda x:(x.pos, x.ref, x.alt))
        # Group FASTA records with the same sequence
        self.group_assignments = {} # File -> group number
        self.groups = defaultdict(list) # Group name => [Sample names]
        counter = 0
        # Compare the DNA between every pair of files
        for index in range(len(self.file_basenames)): 
            file = self.file_basenames[index]
            string = self.consensus_dna_dict[file]  
            for index2 in range(len(self.file_basenames))[index+1:]:
                file2 = self.file_basenames[index2]
                string2 = self.consensus_dna_dict[file2]
                if string == string2:
                    base1 = file.split(".")[0]
                    base2 = file2.split(".")[0]
                    if base1 in self.group_assignments.keys():
                        clr = self.group_assignments[base1]
                        self.group_assignments[base2]= clr
                        self.groups[clr].append(base2)
                    elif base2 in self.group_assignments.keys():
                        clr = self.group_assignments[base2]
                        self.group_assignments[base1] = clr
                        self.groups[clr].append(base1)
                    else:
                        self.group_assignments[base1] = counter
                        self.group_assignments[base2] = counter
                        self.groups[counter].append(base1)
                        self.groups[counter].append(base2)
                        counter += 1

    def make_metadata_clusters(self, metadata_path, has_column_names=False):
        """
        Creates two mappings: (1) each sample to their cluster, and (2) each
        cluster to their samples.

        Parameters
        ----------
        metadata_path: str
            Path to TSV file containing sample metadata (cluster names, etc)
        has_column_names: boolean
            Whether the first row of the TSV file contains the column names
        """
        file = open(metadata_path)
        read_tsv = csv.reader(file, delimiter="\t")
        if has_column_names:
            next(read_tsv)
        for line in read_tsv:
            basename = line[0].strip()
            group_id = line[1]
            self.metadata_cluster_assignments[basename]=group_id
            self.metadata_clusters[group_id].append(basename)

    def create_consensus_sequences(self):
        """
        Returns absolute path to the folder where consensus sequences are placed.
        """
        # Generate fasta file and put FASTA files in "sequences"folder
        foldername = os.path.join(self.output_directory, "consensus", "sequences")
        Path(foldername).mkdir(parents=True, exist_ok=True)
        for filename in os.listdir(self.vcf_directory):
            if filename.endswith(".vcf"):
                filepath = os.path.join(self.vcf_directory, filename)
                vcf2fasta(
                    filepath, 
                    self.reference_fasta_path, 
                    output_dir=foldername,
                    min_AF=0, # make param?
                    max_AF=1,
                    parse_type='biallelic',
                    masks=None, # make param?
                    mask_type='hide', # make param?
                )
        return os.path.abspath(foldername)
        

    def create_isnv_embedded_sequences(self, min_AF=MIN_AF):
        """
        Substitute in the second most abundant variants into the consensus
        sequence of each sample. 

        Parameters
        ----------
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV

        Returns
        -------
        str
            Absolute path to the directory containing the generated consensus
            FASTA files
        """
        output_dir_isnv = os.path.join(self.output_directory, "isnv")
        parsnp_isnv = os.path.join(output_dir_isnv, 'parsnp')
        fasta_dir_isnv = os.path.join(output_dir_isnv, "sequences")
        Path(os.path.join(output_dir_isnv)).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(parsnp_isnv)).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(fasta_dir_isnv)).mkdir(parents=True, exist_ok=True)
        # Write down isnv embedded sequences
        consensus_isnv_address = os.path.join(self.output_directory, "isnv", "sequences")
        # Make the consensus_isnv_address folder if it doesn't exist
        Path(os.path.join(consensus_isnv_address)).mkdir(parents=True, exist_ok=True)
        # Create FASTA file for each sample containing the second most abundant variants
        for basename in self.file_basenames:
            if "ref" not in basename:
                dna_seq = self.consensus_dna_dict[basename] # original consensus sequence
                vcf_dict = extract_variants_from_vcf(os.path.join(self.vcf_directory, basename+".vcf"))
                for pos, nts in vcf_dict.items():
                    highest_freq = 0
                    max_freq = max([info[0] for nt, info in nts.items()])
                    kept_var = None
                    for nt, info in nts.items():
                        freq = info[0]
                        if freq > highest_freq and freq>min_AF and freq != max_freq: # minor variant (not part of the consensus)
                            highest_freq = freq
                            kept_var = nt
                    if kept_var:
                        dna_seq = dna_seq[:int(pos)-1]+kept_var+dna_seq[int(pos):]
                handle = open(os.path.join(consensus_isnv_address, "%s.isnv.fasta"%(basename)), "w")
                handle.write(">%s\n%s\n"%(basename,dna_seq)) 
        return os.path.abspath(fasta_dir_isnv)

    def _extract_variant(self, address, name):
        """
        Parses VCF file and returns a dictionary of genomic position => variants

        Parameters
        ----------
        address : str
            Path to a directory
        name : str
            Name of a VCF file in the directory specified by "address"
        
        Returns
        -------
        dict
        
            {
                position: [
                    (
                        allele, 
                        allele frequency,
                        read depth
                    ),
                ]
            }
        """
        vcf_reader = vcf.Reader(open(os.path.join(address,name), 'r'))
        allele_dict = defaultdict(set)
        for record in vcf_reader:
            # AF: Allele Frequency, DP: Read Depth
            allele_dict[int(record.POS)].add((str(record.ALT[0]),record.INFO["AF"],record.INFO["DP"]))
            allele_dict[int(record.POS)].add((str(record.REF),1-record.INFO["AF"],record.INFO["DP"]))
        return allele_dict

    def construct_tree(self, tree_path, root_wildcard=None):
        """
        Constructs a phylogenetic tree with nodes colored based on metadata
        or matching consensus sequences.

        Parameters
        ----------
        tree_path : str
            Path to a .tree file representing the phylogenetic tree containing all samples
        root_wildcard : str
            Substring that is unique to the name of the root node.
        
        Returns
        -------
        ToyTree
            The tree rooted at the provided root
        """
        tree = toytree.tree(tree_path)
        rtre = tree.root(wildcard=root_wildcard)
        # Set color of each node based on the same DNA string
        for node in rtre.treenode.traverse():
            if node.is_leaf():
                basename = str(node).replace("'", "").lstrip(" \n\r\t-").split(".")[0]
                if basename in self.group_assignments.keys():
                    node.add_feature('color', pallete[self.group_assignments[basename]])            
                else:
                    node.add_feature('color', "lightgrey")  
                # Set the node label (in the tree) to the cluster name
                if self.metadata_cluster_assignments:
                    if "ref" not in str(node):
                        label = self.metadata_cluster_assignments[basename]
                        nodesize = 14 if len(label) < 3 else 16
                        node.add_feature('nlabel', self.metadata_cluster_assignments[basename])
                        node.add_feature('size', nodesize)
                    else:
                        node.add_feature('nlabel',"Ref")
                        node.add_feature('size', 14)
                else:
                    node.add_feature('size', 14)
                    if "ref" not in str(node):
                        if basename in self.group_assignments:
                            node.add_feature('nlabel', self.group_assignments[basename])
                    else:
                        node.add_feature('nlabel',"Ref")
            else:
                node.add_feature('color', "lightgrey")
                node.add_feature('size', 0.5)
                node.add_feature('nlabel',"")
        return rtre
    
    def plot_tree(self, rtre, render_type=None, width=500, height=None):
        """
        Plot a phylogenetic tree with the nodes colored and labeled
        based on tree properties.

        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        render_type : {None, "html", "svg"}, optional
            Type of file to save the tree plot as
        width : int or None, optional
            Width of the tree in pixels
        height : int or None, optional
            Height of the tree in pixels
        """
        # store color list with values for tips and root
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        # draw tree with node colors
        # get canvas and axes with tree plot
        canvas, _, _ = rtre.draw(
            tip_labels_align=True,
            node_colors=colors, 
            node_sizes=sizes,
            node_labels=nlabel,
            width=width,
            height=height)
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "phylogenetic_tree.html")
            toyplot.html.render(canvas, file_path)
            print("Tree visualization has been saved to {}.".format(file_path))
        if render_type == "svg":
            file_path = os.path.join(self.output_directory, "phylogenetic_tree.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree visualization has been saved to {}.".format(file_path))

    def _construct_consensus_matrix(self, rtre):
        """
        Create a matrix of 0s (blue) and 1s (red) as the "base" of the 
        consensus sequence heat map.

        Parameters
        ----------
        rtre : Toytree
            Its tip labels will determine the order of rows in the matrix
        
        Returns
        -------
        List
            Matrix of 0s and 1s where rows correspond to nodes
            and columns to SNPs
        List
            x-axis tick labels
        List
            y-axis tick labels
        """
        xlabel = []
        for item in list(self.total_snps):
            xlabel.append(str(item))  
        x = []
        ylabel = []
        for consensus_file in list(rtre.get_tip_labels())[::-1]:
            basename = consensus_file.split(".")[0]
            values = self.snps[basename]
            sub_snps = [0]*len(self.total_snps)
            for value in values:
                sub_snps[list(self.total_snps).index(value)] = 1
            x.append(sub_snps)
            ylabel.append(consensus_file)
        # Flip array in the up/down direction
        matrix = np.flipud(np.asarray(x))
        return matrix, xlabel, ylabel

    def plot_snp_heatmap(
            self,
            rtre,
            min_AF=MIN_AF,
            render_type=None,
            width=400,
            height=None,
            legend_bounds=None,
            metadata_path=None,
            column_name=None):
        """
        Plot a phylogenetic tree and SNP heat map.
        Heat map legend:
            Blue = Consensus sequence matches the reference at position x.
            Red = Consensus sequence differs from the reference at position x.

        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        min_AF : float, optional
            Minimum allele frequency to detect an SNP
        render_type : {None, "html", "svg"}, optional
            Type of file to save the plot as
        width : int or None, optional
            Width of the tree in px
        height : int or None, optional
            Height of the tree in px
        legend_bounds : (xmin, xmax, ymin, ymax) tuple, optional
          Use the bounds property to position / size the image by specifying the
          position of each of its boundaries.  Assumes CSS pixels if units
          aren't provided, and supports all units described in :ref:`units`,
          including percentage of the canvas width / height.
          Ex. ("10%", "-10%", "75%", "90%")
        metadata_path : str
            Path to TSV file containing sample metadata (cluster names, etc)
        column_name : str
            Name of the column containing metadata
        """
        matrix, xlabel, _ = self._construct_consensus_matrix(rtre)
        # phylogenetic tree + heatmap visualization
        # scale tree to be 2X length of number of matrix cols
        ctree = rtre.mod.node_scale_root_height(matrix.shape[1]*2)
        # get canvas and axes with tree plot
        # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
        # 29 samples: height = 480 px
        # 35 SNPs: width = 900 px
        nnodes = matrix.shape[0]
        ncols = len(xlabel)
        # Use proportions to calculate the height of the canvas.
        # A tree with 29 nodes is 480 px tall.
        height = height or 480 * nnodes / 29
        x_ticks_height = 70
        ax0_width = width # tree width
        # A heat map with 35 SNPs is 440 px wide.
        ax1_x_start = ax0_width + 30 
        ax1_width = 440 * ncols / 35
        wid_end = int(ax1_x_start + ax1_width + 150)
        canvas = toyplot.Canvas(width=wid_end, height=height)
        #bounds (left,right,bottom,top)
        ax0 = canvas.cartesian(bounds=(0, ax0_width, 70, height - x_ticks_height + 13),)
        # subtract 10 from wid_end to prevent the scatterplot from being cut off.
        ax1 = canvas.cartesian(bounds=(ax1_x_start + 20, wid_end-10, 80,height - x_ticks_height))
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        #print(len(colors),len(sizes),len(nlabel))
        ctree.draw(node_colors=colors, node_sizes=sizes,node_labels=nlabel,\
                axes=ax0, tip_labels=True, tip_labels_align=True)
        ax0.show = False
        # Add metadata column
        if metadata_path:
            metadata_legend = {}
            metadata = {}
            file = open(metadata_path)
            read_tsv = csv.reader(file, delimiter="\t")
            column_idx = -1 # Line 1
            count = 0 # Determines which color to use for a cluster
            groups = {} # group_id => "count" value
            for line in read_tsv:
                if column_idx == -1:
                    # Find the desired column
                    column_idx = line.index(column_name)
                else:
                    basename = line[0].strip()
                    group_id = line[column_idx]
                    metadata[basename] = group_id
                    if group_id not in groups.keys():
                        count += 1
                        groups[group_id] = count
                    metadata_legend[group_id] = pallete[len(pallete) - groups[group_id]]
            col_colors = []
            for consensus_file in list(rtre.get_tip_labels()):
                basename = consensus_file.split(".")[0]
                if "ref" in basename: # reference
                        col_colors.append("#ffffff")
                else:
                    group_id = metadata[basename]
                    col_colors.append(metadata_legend[group_id])
            ax1.scatterplot(
                np.repeat(0, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
            # Create legend for metadata
            legend_bounds_temp = list(legend_bounds)
            metadata_legend_length = len(metadata_legend)
            new_height = ((legend_bounds[3] - legend_bounds[2]) * metadata_legend_length) / (metadata_legend_length + 2)
            legend_bounds_temp[3] = legend_bounds[2] + new_height
            new_legend_bounds = tuple(legend_bounds_temp)
            # recalculate legend_bounds for the "heatmap" legend
            legend_bounds_temp = list(legend_bounds)
            padding = min(new_height * 1.2, 50)
            legend_bounds_temp[2] = legend_bounds[2] + new_height + padding
            legend_bounds_temp[3] = legend_bounds[3] + padding
            legend_bounds = tuple(legend_bounds_temp)
            markers = []
            for group_id, color in metadata_legend.items():
                markers.append((group_id, toyplot.marker.create(shape="s", mstyle={"fill":color})))
            canvas.legend(markers,
                bounds=new_legend_bounds,
                label=column_name
            )
        # add n columns of data to the heatmap
        blue_shade = "#4477AA"
        red_shade = "#CC3311"
        plt = [blue_shade,red_shade]
        offset = 0
        if metadata_path:
            offset = 1
        for col in range(ncols):
            # select the column of data
            data = matrix[:, col]
            col_colors = [plt[i] for i in list(data)]
            # plot the data column
            ax1.scatterplot(
                np.repeat(col + offset, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
        ax1.x.ticks.show = True
        ax1.x.ticks.labels.angle = 90
        ax1.x.ticks.locator = toyplot.locator.Explicit(range(offset, len(xlabel) + offset),xlabel)
        ax1.y.show = False
        # https://toyplot.readthedocs.io/en/stable/markers.html#markers
        marker_a = toyplot.marker.create(shape="s", mstyle={"fill":blue_shade})
        marker_b = toyplot.marker.create(shape="s", mstyle={"fill":red_shade})
        # https://toyplot.readthedocs.io/en/stable/labels-and-legends.html
        canvas.legend([
                ("Reference", marker_a),
                ("SNP", marker_b),
            ],
            bounds=legend_bounds,
            label="Heatmap"
        )
        # stretch domain to fit long tip names
        #axes.x.domain.max =5 # fix this
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "consensus", "snp_heatmap.html")
            toyplot.html.render(canvas, file_path)
            print("Tree + SNP heatmap visualization has been saved to {}.".format(file_path))
        if render_type == "svg":
            file_path = os.path.join(self.output_directory, "consensus", "snp_heatmap.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree + SNP heatmap visualization has been saved to {}.".format(file_path))
        #plt.show()

    def infer_phylogeny(self, fasta_files, output_dir, threads=THREADS):
        """
        Infer the phylogeny of a group of sequences given their FASTA files.

        Parameters
        ----------
        fasta_files: str
            Path to folder of FASTA files to align
        output_dir: str
            Path to folder to place Parsnp and RAxML outputs into
        threads: int, optional
            Number of threads to use when running Parsnp and RAxML

        Returns
        -------
        str
            Path to the tree file generated by RAxML
        str
            Name of the reference sequence file
        """
        Path(os.path.join(output_dir, "parsnp")).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(output_dir, "raxml")).mkdir(parents=True, exist_ok=True)

        parsnp = os.path.join(output_dir, 'parsnp')
        
        tmpdata = FastaAligner(fasta_files)
        tmpdata.align(self.reference_fasta_path, output_dir=parsnp, threads=threads)
        # Parse multifasta
        multifasta = os.path.join(parsnp, 'parsnp.mfa')
        seqs = MultiFastaParser(multifasta)
        # Assume ref seq is first record in multifasta (is the case w/ parsnp)
        refname = seqs.records[0].name.split(".")[0]
        seqs.infer_phylogeny(output_dir=os.path.join(output_dir, "raxml"), threads=threads)
        raxml_newick = os.path.join(output_dir, "raxml", "RAxML_bestTree.tree")
        return raxml_newick, refname

    def _construct_isnv_matrix(self, rtre, min_AF=MIN_AF, show_snps=False):
        """
        Creates matrix that is the numerical representation of the iSNV heat map.
        
        Parameters
        ----------
        rtre : ToyTree
            Tree constructed from iSNV files
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        show_snps : boolean, optional
            Whether to set the matrix equal to 1 for iSNVs that have reached
            consensus in a sample. This will cause the heatmap to display a 
            red square for that sample at that iSNV.

        Returns
        -------
        List
            Matrix of allele frequencies where rows correspond to nodes
            and columns to iSNVs
        List
            All iSNVs within the sequences that make up the tree nodes
        """
        x, total_isnvs, file_isnvs = [],set(),defaultdict(list)
        files = []
        for f in list(rtre.get_tip_labels())[::-1]: 
            basename = f.split(".")[0] 
            files.append(basename)
        for basename in files:
            if "ref" not in basename:
                vcf_dict = extract_variants_from_vcf(os.path.join(self.vcf_directory, basename+".vcf"))
                #print(f"{basename}")
                for pos,nts in vcf_dict.items():
                    highest_freq = 0
                    max_freq = max([info[0] for nt, info in nts.items()])
                    kept_var = None
                    for nt, info in nts.items():
                        freq = info[0]
                        ref = self.reference[int(pos)-1]
                        #low frequency variants
                        if freq > highest_freq and freq>min_AF and freq != max_freq and nt != ref:
                            highest_freq = freq
                            kept_var = nt
                    if kept_var:
                        #lowfreq: pos,nucleotide
                        trie = Variant(pos, self.reference[int(pos)-1], kept_var, highest_freq)
                        total_isnvs.add(trie)
                        # isnv for each file: (pos&nucleotide, frequency)
                        #print(f"\t(position {pos}): Chose {trie.str2()} out of: {nts}")
                        file_isnvs[basename].append(trie)
        total_isnvs = sorted(list(total_isnvs), key=lambda x:(x.pos, x.ref, x.alt)) 
        for f in list(rtre.get_tip_labels())[::-1]:
            basename = f.split(".")[0].replace("'","")
            if basename in file_isnvs.keys():
                # row heatmap
                sub_x = [0]*len(total_isnvs)
                for isnv in file_isnvs[basename]:
                    sub_x[list(total_isnvs).index(isnv)] = isnv.freq
                # TODO(ahwang): This caused a LOT of cells to be red in the malta data.
                if show_snps:
                    for snp in self.snps[basename]:
                        # Check if any of the sample's SNPs are actually iSNVs in other samples
                        # If so, set the matrix value to 1
                        if snp in total_isnvs:
                            sub_x[list(total_isnvs).index(snp)] = 1
                x.append(sub_x)
            else:
                x.append([0]*len(total_isnvs))
        
        matrix = np.asarray(x)
        return matrix, total_isnvs

    def plot_isnv_heatmap(
            self, 
            rtre, 
            min_AF=MIN_AF, 
            show_snps=False,
            render_type=None, 
            width=500, 
            height=None,
            legend_bounds=("10%", "-10%", "75%", "90%"),
            metadata_path=None,
            column_name=None
    ):
        """
        Plot a phylogenetic tree and iSNV heat map.
        Heat map legend:
            Grey = the iSNV is not present in the sample
            Red = the iSNV is the majority allele in the sample
            Green -> Yellow = direction of increasing allele frequency of the iSNV
        
        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        show_snps : boolean, optional
            Whether to set the matrix equal to 1 for iSNVs that have reached
            consensus in a sample. This will cause the heatmap to display a 
            red square for that sample at that iSNV.
        render_type : {"html", "svg"}, optional
            Type of file to save the plot as
        width : int or None, optional
            Width of the tree in pixels
        height : int or None, optional
            Height of the tree in pixels
        legend_bounds : (xmin, xmax, ymin, ymax) tuple, optional
          Use the bounds property to position / size the image by specifying the
          position of each of its boundaries.  Assumes CSS pixels if units
          aren't provided, and supports all units described in :ref:`units`,
          including percentage of the canvas width / height.
        metadata_path : str
            Path to TSV file containing sample metadata (cluster names, etc)
        column_name : str
            Name of the column containing metadata
        """
        isnv_matrix, total_isnvs = self._construct_isnv_matrix(rtre, min_AF, show_snps=show_snps)
        xlabel = []
        for isnv in total_isnvs:
            xlabel.append(str(isnv))
        # store color list with values for tips and root
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        # draw tree with node colors
        # scale tree to be 2X length of number of matrix cols
        ctree = rtre.mod.node_scale_root_height(isnv_matrix.shape[1]*2)
        # get canvas and axes with tree plot
        # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
        nnodes = isnv_matrix.shape[0]
        ncols = isnv_matrix.shape[1]
        # Use proportions to calculate the height of the canvas.
        height = height or 480 * nnodes / 29 # A tree with 29 nodes is 480 px tall.
        x_ticks_height = 70 # Height of the x-axis labels (iSNVs)
        ax0_width = width # Width of the tree
        ax1_x_start = ax0_width + 30 # Allow extra space for tip labels
        ax1_width = 885 * ncols / 55 # A heat map with 55 iSNVs is ~ 885 px wide.
        wid_end = int(ax1_x_start + ax1_width + 150)
        padding_for_legend = 100
        canvas = toyplot.Canvas(width=wid_end + padding_for_legend, height=height)
        # bounds (left,right,bottom,top)
        # Add 13 to ymax to "stretch" the tree to align with the heatmap.
        ax0 = canvas.cartesian(bounds=(0, ax0_width, 70, height - x_ticks_height + 13))
        # Subtract 10 from wid_end to prevent the scatterplot from being cut off.
        ax1 = canvas.cartesian(bounds=(ax1_x_start + 20,wid_end-10, 85, height - x_ticks_height))
        ###### TREE ######
        ctree.draw(node_colors=colors, node_sizes=sizes,node_labels=nlabel,\
                axes=ax0, tip_labels=True, tip_labels_align=True)
        ax0.show = False
        ###### METADATA LAYER #####
        if metadata_path:
            metadata_legend = {}
            metadata = {}
            file = open(metadata_path)
            read_tsv = csv.reader(file, delimiter="\t")
            column_idx = -1 # Line 1
            count = 0 # Determines which color to use for a cluster
            groups = {} # group_id => "count" value
            for line in read_tsv:
                if column_idx == -1:
                    # Find the desired column
                    column_idx = line.index(column_name)
                else:
                    basename = line[0].strip()
                    group_id = line[column_idx]
                    metadata[basename] = group_id
                    if group_id not in groups.keys():
                        count += 1
                        groups[group_id] = count
                    metadata_legend[group_id] = pallete[len(pallete) - groups[group_id]]
            col_colors = []
            for isnv_file in list(rtre.get_tip_labels()):
                basename = isnv_file.split(".")[0]
                if "ref" in basename: # reference
                        col_colors.append("#ffffff")
                else:
                    group_id = metadata[basename]
                    col_colors.append(metadata_legend[group_id])
            ax1.scatterplot(
                np.repeat(0, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
            # Create legend for metadata
            legend_bounds_temp = list(legend_bounds)
            metadata_legend_length = len(metadata_legend)
            new_height = ((legend_bounds[3] - legend_bounds[2]) * metadata_legend_length) / (metadata_legend_length + 2)
            legend_bounds_temp[3] = legend_bounds[2] + new_height
            new_legend_bounds = tuple(legend_bounds_temp)
            # recalculate legend_bounds for the "heatmap" legend
            legend_bounds_temp = list(legend_bounds)
            padding = min(new_height * 1.2, 50)
            legend_bounds_temp[2] = legend_bounds[2] + new_height + padding
            legend_bounds_temp[3] = legend_bounds[3] + padding
            legend_bounds = tuple(legend_bounds_temp)
            markers = []
            for group_id, color in metadata_legend.items():
                markers.append((group_id, toyplot.marker.create(shape="s", mstyle={"fill":color})))
            canvas.legend(markers,
                bounds=new_legend_bounds,
                label=column_name
            )
        ###### HEATMAP ######
        ncols = len(total_isnvs)
        colormap = toyplot.color.brewer.map("GreenYellow") # Used to be GreenYellowRed, BlueYellowRed
        defaultmap = toyplot.color.brewer.map("Set1")
        default_color = "#000000" # formerly dark grey: #888888"
        offset = 0
        if metadata_path:
            offset = 1
        for col in range(ncols):
            # select the column of data
            data = isnv_matrix[:, col][::-1]
            #print(f"Column {col} data: {data}")
            col_colors = []
            for i in list(data):
                if i == 0:
                    col_colors.append(toyplot.color.css(default_color)) 
                elif i == 1:
                    # red if the shared lofreq variant IS the majority allele for this sample
                    col_colors.append(defaultmap.colors(0,0,1)) 
                else:
                    col_colors.append(colormap.colors(math.log(i + .01),math.log(0.01),math.log(0.5)))
            # plot the data column
            ax1.scatterplot(
                np.repeat(col + offset, rtre.ntips), # repeating X-value
                np.arange(rtre.ntips), # Y-values
                marker='s',
                size=10, 
                color=col_colors)
            ax1.x.ticks.show = True
            ax1.x.ticks.labels.angle = 90
            ax1.x.ticks.locator = toyplot.locator.Explicit(range(offset, len(xlabel) + offset),xlabel)
            ax1.y.show = False
            # stretch domain to fit long tip names
            # axes.x.domain.max =10 # ahwang: fix this
        colors = [default_color] + list(toyplot.color.brewer.map("GreenYellow")._palette._colors) + [defaultmap.colors(0)]
        full_palette=toyplot.color.Palette(colors)
        full_colormap = toyplot.color.LinearMap(
            palette=full_palette,
            domain_min=0,
            domain_max=1
        )
        ticklocator = toyplot.locator.Explicit(
            locations=[0,.9,1],
            labels=["0", ".5", "1"]
        )
        canvas.color_scale(
                    full_colormap, 
                    bounds=legend_bounds, 
                    label="Allele Frequency",
                    ticklocator=ticklocator)
        # ax1.color_scale(full_colormap, 
        #                 label="Allele Frequency",
        #                 tick_locator=ticklocator
        # )
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "isnv", "isnv_heatmap.html")
            toyplot.html.render(canvas, file_path)
            print("Tree + iSNV heat map visualization has been saved to {}.".format(file_path))
        elif render_type == "svg":
            file_path = os.path.join(self.output_directory, "isnv", "isnv_heatmap.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree + iSNV heat map visualization has been saved to {}.".format(file_path))

    def align_isnv_clusters(self, threads=THREADS):
        """
        Infer the phylogeny of iSNV embedded sequences by sample cluster.

        Parameters
        ----------
        threads : int, optional
            Number of threads to use when running Parsnp and RAxML

        Returns
        -------
        str
            Path to folder containing one folder of tree files per cluster.
        """
        # If metadata is present:
        # 	Create a collection of folders, each representing a cluster based on the metadata. 
        # 	Copy over these iSNV embedded sequences into each folder.
        Path(os.path.join(self.output_directory, "isnv", "groups")).mkdir(parents=True, exist_ok=True)
        groups_isnv = defaultdict(list)
        if self.metadata_cluster_assignments:
            for basename, cluster in self.metadata_cluster_assignments.items():
                groups_isnv[cluster].append(basename+".isnv.fasta")
        else:
            # Create groups based on same sequence.
            for basename, group in self.group_assignments.items():
                groups_isnv[group].append(basename + ".isnv.fasta")
        # Copy over consensus iSNV FASTA files to new folders based on the cluster
        for group_id, files in groups_isnv.items():
            #if len(files) > 1:
            for filename in files:
                # TODO: Delete folder if it exists?
                path = os.path.join(self.output_directory, "isnv", "groups", str(group_id))
                Path(path).mkdir(parents=True, exist_ok=True)
                # copy the 'TRANSM-38.isnv.fasta' files from consensus_isnv/ folder to consensus_isnv_group/%s/%s
                os.system("cp %s %s"%(os.path.join(self.output_directory, "isnv", "sequences", filename), os.path.join(self.output_directory,"isnv", "groups", str(group_id), filename)))
        output_dir_isnv = os.path.join(self.output_directory, "isnv")
        for group in os.listdir(os.path.join(output_dir_isnv, "groups")):
            if os.path.isdir(os.path.join(output_dir_isnv, "groups", group)):		
                consensus_isnv_address = os.path.join(output_dir_isnv, "groups", group)
                self.infer_phylogeny(
                    consensus_isnv_address, 
                    os.path.join(self.output_directory, "isnv", "groups", group),
                    threads=threads
                )
        return os.path.join(output_dir_isnv, "groups")

    def plot_isnv_clusters(
            self,
            cluster_folder_path,
            min_AF=MIN_AF,
            render_type=None,
            show_legend=False):
        """
        Plot the phylogenetic tree and iSNV heat map of each cluster.

        Parameters
        ----------
        cluster_folder_path : str
            Path to folder containing one folder of tree files per cluster.
            The tree files have to be in a folder titled "raxml",
            i.e. "{cluster_folder_path}/A/raxml"
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        render_type : {None, "html", "svg"}, optional
            Type of file to save the plots as
        show_legend : boolean, optional
            Whether to display the legend mapping colors to iSNV frequencies
        """
        for cluster in os.listdir(cluster_folder_path):
            filename = os.path.join(cluster_folder_path, cluster, "raxml", "RAxML_bestTree.tree")
            if not os.path.isfile(filename):
                print("{} does not exist!".format(filename))
                continue
            rtre = self.construct_tree(tree_path=filename, root_wildcard="SARS")
            matrix, total_isnvs = self._construct_isnv_matrix(rtre, min_AF=MIN_AF)
            # scale tree to be 2X length of number of matrix cols
            rtre = rtre.mod.node_scale_root_height(matrix.shape[1]*1.5)
            colors = rtre.get_node_values('color', show_root=1, show_tips=1)
            sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
            nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
            num_nodes = len(list(rtre.get_tip_labels()))
            # get canvas and axes with tree plot
            # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
            wid_end = 300+ 15*len(total_isnvs)
            hght = 35*num_nodes 
            x_ticks_height = 70 # Height of the x-axis labels (iSNVs)
            padding_for_legend = 0
            if show_legend:
                padding_for_legend = 50
            canvas = toyplot.Canvas(width=wid_end, height=hght + x_ticks_height + padding_for_legend)
            #bounds (left,right,bottom,top)
            ax0_height = hght - x_ticks_height
            ax0 = canvas.cartesian(bounds=(0, 300, 0, ax0_height), padding=5, ymin=0, ymax=num_nodes+2)
            ax1 = canvas.cartesian(bounds=(300+30,wid_end-10, 0,hght - x_ticks_height), padding=10, ymin=0, ymax=num_nodes+2)
            rtre.draw(axes=ax0, tip_labels=True, tip_labels_align=True, node_colors=colors, node_sizes=sizes,node_labels=nlabel)
            ax0.show = False
            # add n columns of data (here random data)
            ncols = len(total_isnvs)
            colormap = toyplot.color.brewer.map("GreenYellow")
            defaultmap = toyplot.color.brewer.map("Set1")
            for col in range(ncols):
                # select the column of data
                data = matrix[:, col][::-1]
                col_colors = []
                for i in list(data):
                    if i == 0 :
                        col_colors.append(toyplot.color.css("#888888"))
                    elif i == 1:
                        col_colors.append(defaultmap.colors(0,0,1))
                    else:
                        col_colors.append(colormap.colors(math.log(i+0.01),math.log(0.01),math.log(0.5)))
                # plot the data column
                ax1.scatterplot(np.repeat(col, rtre.ntips), # repeating X-value
                                np.arange(rtre.ntips), # Y-values
                                marker='s',
                                size=10, 
                                color=col_colors)
            ax1.x.ticks.show = True
            ax1.x.ticks.labels.angle = 90
            ax1.x.ticks.locator = \
            toyplot.locator.Explicit(range(len(total_isnvs)),total_isnvs)
            ax1.y.show = False
            if show_legend:
                colors = ["#888888"] + list(toyplot.color.brewer.map("GreenYellow")._palette._colors) + [defaultmap.colors(0)]
                full_palette=toyplot.color.Palette(colors)
                full_colormap = toyplot.color.LinearMap(
                    palette=full_palette,
                    domain_min=0,
                    domain_max=1
                )
                canvas.color_scale(
                    full_colormap, 
                    bounds=("10%", "25%", ax0_height, ax0_height + 50), # legend_bounds, 
                    label="Allele Frequency")
            if render_type == "html":
                toyplot.html.render(canvas, os.path.join(cluster_folder_path, "{}.html".format(cluster)))
            if render_type == "svg":
                toyplot.svg.render(canvas, os.path.join(cluster_folder_path, "{}.svg".format(cluster)))

def plot_phylotree_all(
        vcf_directory, 
        reference_fasta, 
        metadata_path=None,
        colors=COLORS, 
        output_dir='TransmissionViz', 
        min_AF=0,
        render_type=None,
        width=500,
        height=None,
        threads=THREADS,
        masks=None,
        mask_type='hide'
):
    """
    Generate figures on the full tree and subtree phylogenies given
    a directory of VCF files and a reference genome.

    It should be noted that running this provides less customizablity than calling the
    PhyloTree class methods individually.

    Parameters
    ----------
    vcf_directory : str
        Path to directory containing VCF files
    reference_fasta : str
        Path to reference genome FASTA file
    metadata_path : str, optional
        Path to a TSV file labeling samples with metadata
    colors : list, optional
        List of color strings to color nodes with
    output_dir : str, optional
        Path to directory to place all outputs in
    min_AF : float, optional
        Minimum allele frequency to detect a variant (SNP or iSNV)
    render_type : {None, "html", "svg"}, optional
        Type of file to save the tree plot as
    width : int or None, optional
        Width of the tree plots in pixels
    height : int or None, optional
        Height of the tree plots in pixels 
    threads : int, optional
        Number of threads to use when running Parsnp and RAxML
    masks : str, optional (unused)
        TODO(ahwang): Complete masks defn
    mask_type : str, optional (unused)
        TODO(ahwang): Complete mask_type defn
    """

    # Check if path exists
    if not os.path.exists(vcf_directory):
        raise FileNotFoundError(f"Directory does not exist: {vcf_directory}")
    if not os.path.exists(reference_fasta):
        raise FileNotFoundError(f"File does not exist: {reference_fasta}")
    
    # Run normal tree stuff
    tree = PhyloTree(output_dir, reference_fasta, vcf_directory)
    # Group samples based on DNA sequence: map the base name => group number
    if metadata_path:
        tree.make_metadata_clusters(metadata_path, True)

    consensus_fasta_folder = tree.create_consensus_sequences()
    raxml_newick, refname = tree.infer_phylogeny(
        consensus_fasta_folder, 
        os.path.join(output_dir, "consensus"),
        threads=threads) # 1
    
    rtre = tree.construct_tree(tree_path=raxml_newick, root_wildcard=refname)
    tree.plot_tree(rtre, render_type=render_type, width=width, height=height)
    tree.plot_snp_heatmap(rtre, min_AF=min_AF, render_type=render_type)

    # Create iSNV-embedded sequence FASTA files. Align them.
    isnv_fasta_folder = tree.create_isnv_embedded_sequences()
    raxml_newick_isnv, refname = tree.infer_phylogeny(
        isnv_fasta_folder, 
        os.path.join(output_dir, "isnv"),
        threads=threads)
    rtre2 = tree.construct_tree(tree_path = raxml_newick_isnv, root_wildcard=refname)
    tree.plot_isnv_heatmap(rtre2, min_AF=min_AF, render_type=render_type)

    # Run RAxML on each iSNV cluster.
    cluster_folder_path = tree.align_isnv_clusters(threads=threads)

    # Visualize the clusters
    tree.plot_isnv_clusters(cluster_folder_path, min_AF=min_AF, render_type=render_type)

    # Get rid of tmp directory
    #shutil.rmtree(tmp_path, ignore_errors=True)