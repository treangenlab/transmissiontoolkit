### IMPORTS ###
import os
import csv
import matplotlib.pyplot as plt
import numpy as np
import toytree
import toyplot
import toyplot.svg
import math

from trakit.vcfParsing import extract_variants_from_vcf
from trakit.genomeGen import vcf2fasta
from trakit.trakitUtils import *
from pathlib import Path
from collections import defaultdict

# Using package resources
import importlib
import importlib.resources as resources

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

class PhyloTree:
    """
    Class for visualizing phylogenies using toytree

    Attributes
    ----------
    output_directory : str
        Path to folder to place phylogenetic tree related files
    reference_fasta : str
        Path to reference genome FASTA file
    vcf_directory : str
        Path to a directory containing VCF files
    metadata_path : str, optional
        Path to a TSV file labeling samples with metadata
    has_column_names: boolean, optional
        Whether the first row of the TSV file contains the column names
    """
    def __init__(self, output_directory, reference_fasta, vcf_directory, metadata_path=None, has_column_names=False):
        self.output_directory = output_directory
        self.reference_fasta_path = reference_fasta
        self.reference_fasta_base = os.path.basename(reference_fasta)
        self.vcf_directory = vcf_directory
        self.metadata_path = metadata_path
        self.metadata_cluster_assignments = {} # Sample name => Cluster name
        self.metadata_clusters = defaultdict(list) # Cluster name => [Sample names]
        if self.metadata_path:
            self.make_metadata_clusters(metadata_path, has_column_names)
        # Reference genome DNA sequence
        self.reference = ""
        file1 = open(self.reference_fasta_path)
        next(file1)
        for line in file1:
            self.reference+=line.rstrip()
        # "consensus/sequences" folder for storing the consensus sequence FASTA of each sample
        Path(os.path.join(self.output_directory, "consensus", "sequences")).mkdir(parents=True, exist_ok=True)
        self.file_basenames = [] # List of consensus fasta file base names without the extensions
        self.consensus_dna_dict = {} # consensus fasta file name => consensus DNA sequence. 
        # This includes the consensus sequences for each sample AND the reference.
        self.consensus_dna_dict[self.reference_fasta_base.split(".")[0]] = self.reference
        # List of SNPs observed from all samples
        self.total_snps = set()
        # Map each sample to a list of its SNPs
        self.snps = defaultdict(list)
        self.file_basenames.append(self.reference_fasta_base.split(".")[0])
        for file in os.listdir(self.vcf_directory):
            if file.endswith(".vcf"):
                file_dict = extract_variants_from_vcf(
                    os.path.join(self.vcf_directory, file))
                # Use the reference genome as the original sequence
                dna_seq = str(self.reference)
                for pos, variants in file_dict.items():
                    highest_freq = 0
                    kept_var = None
                    for variant, info in variants.items():
                        # AF = allele frequency
                        freq = info[0]
                        reference_char = self.reference[int(pos)-1] 
                        # majority allele / SNP
                        if freq > 0.5 and variant != reference_char:
                        #if freq > highest_freq and variant != reference_char:
                            highest_freq = freq
                            kept_var = variant
                    if kept_var:
                        dna_seq = dna_seq[:int(pos)-1]+kept_var+dna_seq[int(pos):]
                        basename = file.split(".")[0]
                        snp = Variant(int(pos), reference_char, kept_var, highest_freq)
                        self.snps[basename].append(snp)
                        self.total_snps.add(snp)  
                # Do not rename the reference FASTA file.
                fasta_filename = "%s.consensus.fasta"%(file.split(".")[0])		
                fasta_basename = fasta_filename.split(".")[0]
                # handle = open(os.path.join(self.output_directory, "consensus", "sequences", fasta_filename), "w") 
                # handle.write(">%s\n%s\n"%(file.split(".")[0],dna_seq))
                self.file_basenames.append(fasta_basename)
                self.consensus_dna_dict[fasta_basename] = dna_seq
        # Sort SNPs by genomic position
        self.total_snps = sorted(list(self.total_snps), key=lambda x:(x.pos, x.ref, x.alt))
        # Group FASTA records with the same sequence
        self.group_assignments = {} # File -> group number
        self.groups = defaultdict(list) # Group name => [Sample names]
        counter = 0
        # Compare the DNA between every pair of files
        for index in range(len(self.file_basenames)): 
            file = self.file_basenames[index]
            string = self.consensus_dna_dict[file]  
            for index2 in range(len(self.file_basenames))[index+1:]:
                file2 = self.file_basenames[index2]
                string2 = self.consensus_dna_dict[file2]
                if string == string2:
                    base1 = file.split(".")[0]
                    base2 = file2.split(".")[0]
                    if base1 in self.group_assignments.keys():
                        clr = self.group_assignments[base1]
                        self.group_assignments[base2]= clr
                        self.groups[clr].append(base2)
                    elif base2 in self.group_assignments.keys():
                        clr = self.group_assignments[base2]
                        self.group_assignments[base1] = clr
                        self.groups[clr].append(base1)
                    else:
                        self.group_assignments[base1] = counter
                        self.group_assignments[base2] = counter
                        self.groups[counter].append(base1)
                        self.groups[counter].append(base2)
                        counter += 1

    def make_metadata_clusters(self, metadata_path, has_column_names=False):
        """
        Creates two mappings: (1) each sample to their cluster, and (2) each
        cluster to their samples.

        Parameters
        ----------
        metadata_path: str
            Path to TSV file containing sample metadata (cluster names, etc)
        has_column_names: boolean
            Whether the first row of the TSV file contains the column names
        """
        file = open(metadata_path)
        read_tsv = csv.reader(file, delimiter="\t")
        if has_column_names:
            next(read_tsv)
        for line in read_tsv:
            basename = line[0].strip()
            group_id = line[1]
            self.metadata_cluster_assignments[basename]=group_id
            self.metadata_clusters[group_id].append(basename)

    def create_consensus_sequences(self):
        """
        Returns absolute path to the folder where consensus sequences are placed.
        """
        # Generate fasta file and put FASTA files in "sequences"folder
        foldername = os.path.join(self.output_directory, "consensus", "sequences")
        Path(foldername).mkdir(parents=True, exist_ok=True)
        for filename in os.listdir(self.vcf_directory):
            if filename.endswith(".vcf"):
                filepath = os.path.join(self.vcf_directory, filename)
                vcf2fasta(
                    filepath, 
                    self.reference_fasta_path, 
                    output_dir=foldername,
                    min_AF=0, # make param?
                    max_AF=1,
                    parse_type='biallelic',
                    masks=None, # make param?
                    mask_type='hide', # make param?
                )
        return os.path.abspath(foldername)
        
    def create_isnv_embedded_sequences(self, min_AF=MIN_AF):
        """
        Substitute in the second most abundant variants into the consensus
        sequence of each sample. 

        Parameters
        ----------
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV

        Returns
        -------
        str
            Absolute path to the directory containing the generated consensus
            FASTA files
        """
        output_dir_isnv = os.path.join(self.output_directory, "isnv")
        parsnp_isnv = os.path.join(output_dir_isnv, 'parsnp')
        fasta_dir_isnv = os.path.join(output_dir_isnv, "sequences")
        Path(os.path.join(output_dir_isnv)).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(parsnp_isnv)).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(fasta_dir_isnv)).mkdir(parents=True, exist_ok=True)
        # Write down isnv embedded sequences
        consensus_isnv_address = os.path.join(self.output_directory, "isnv", "sequences")
        # Make the consensus_isnv_address folder if it doesn't exist
        Path(os.path.join(consensus_isnv_address)).mkdir(parents=True, exist_ok=True)
        # Create FASTA file for each sample containing the second most abundant variants
        for basename in self.file_basenames:
            if "ref" not in basename:
                dna_seq = self.consensus_dna_dict[basename] # original consensus sequence
                vcf_dict = extract_variants_from_vcf(os.path.join(self.vcf_directory, basename+".vcf"))
                for pos, nts in vcf_dict.items():
                    highest_freq = 0
                    max_freq = max([info[0] for nt, info in nts.items()])
                    kept_var = None
                    for nt, info in nts.items():
                        freq = info[0]
                        if freq > highest_freq and freq>min_AF and freq != max_freq: # minor variant (not part of the consensus)
                            highest_freq = freq
                            kept_var = nt
                    if kept_var:
                        dna_seq = dna_seq[:int(pos)-1]+kept_var+dna_seq[int(pos):]
                handle = open(os.path.join(consensus_isnv_address, "%s.isnv.fasta"%(basename)), "w")
                handle.write(">%s\n%s\n"%(basename,dna_seq)) 
        return os.path.abspath(fasta_dir_isnv)

    def _extract_variant(self, address, name):
        """
        Parses VCF file and returns a dictionary of genomic position => variants

        Parameters
        ----------
        address : str
            Path to a directory
        name : str
            Name of a VCF file in the directory specified by "address"
        
        Returns
        -------
        dict
        
            {
                position: [
                    (
                        allele, 
                        allele frequency,
                        read depth
                    ),
                ]
            }
        """
        vcf_reader = vcf.Reader(open(os.path.join(address,name), 'r'))
        allele_dict = defaultdict(set)
        for record in vcf_reader:
            # AF: Allele Frequency, DP: Read Depth
            allele_dict[int(record.POS)].add((str(record.ALT[0]),record.INFO["AF"],record.INFO["DP"]))
            allele_dict[int(record.POS)].add((str(record.REF),1-record.INFO["AF"],record.INFO["DP"]))
        return allele_dict

    def construct_tree(self, tree_path, root_wildcard=None):
        """
        Constructs a phylogenetic tree with nodes colored based on metadata
        or matching consensus sequences.

        Parameters
        ----------
        tree_path : str
            Path to a .tree file representing the phylogenetic tree containing all samples
        root_wildcard : str
            Substring that is unique to the name of the root node.
        
        Returns
        -------
        ToyTree
            The tree rooted at the provided root
        """
        tree = toytree.tree(tree_path)
        rtre = tree.root(wildcard=root_wildcard)
        # Set color of each node based on the same DNA string
        for node in rtre.treenode.traverse():
            if node.is_leaf():
                basename = str(node).replace("'", "").lstrip(" \n\r\t-").split(".")[0]
                if basename in self.group_assignments.keys():
                    node.add_feature('color', pallete[self.group_assignments[basename]])            
                else:
                    node.add_feature('color', "lightgrey")  
                # Set the node label (in the tree) to the cluster name
                if self.metadata_cluster_assignments:
                    if "ref" not in str(node):
                        label = self.metadata_cluster_assignments[basename]
                        nodesize = 14 if len(label) < 3 else 16
                        node.add_feature('nlabel', self.metadata_cluster_assignments[basename])
                        node.add_feature('size', nodesize)
                    else:
                        node.add_feature('nlabel',"Ref")
                        node.add_feature('size', 14)
                else:
                    node.add_feature('size', 14)
                    if "ref" not in str(node):
                        if basename in self.group_assignments:
                            node.add_feature('nlabel', self.group_assignments[basename])
                    else:
                        node.add_feature('nlabel',"Ref")
            else:
                node.add_feature('color', "lightgrey")
                node.add_feature('size', 0.5)
                node.add_feature('nlabel',"")
        return rtre
    
    def plot_tree(self, rtre, render_type=None, width=500, height=None):
        """
        Plot a phylogenetic tree with the nodes colored and labeled
        based on tree properties.

        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        render_type : {None, "html", "svg"}, optional
            Type of file to save the tree plot as
        width : int or None, optional
            Width of the tree in pixels
        height : int or None, optional
            Height of the tree in pixels
        """
        # store color list with values for tips and root
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        # draw tree with node colors
        # get canvas and axes with tree plot
        canvas, _, _ = rtre.draw(
            tip_labels_align=True,
            node_colors=colors, 
            node_sizes=sizes,
            node_labels=nlabel,
            width=width,
            height=height)
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "phylogenetic_tree.html")
            toyplot.html.render(canvas, file_path)
            print("Tree visualization has been saved to {}.".format(file_path))
        if render_type == "svg":
            file_path = os.path.join(self.output_directory, "phylogenetic_tree.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree visualization has been saved to {}.".format(file_path))

    def _construct_consensus_matrix(self, rtre):
        """
        Create a matrix of 0s (blue) and 1s (red) as the "base" of the 
        consensus sequence heat map.

        Parameters
        ----------
        rtre : Toytree
            Its tip labels will determine the order of rows in the matrix
        
        Returns
        -------
        List
            Matrix of 0s and 1s where rows correspond to nodes
            and columns to SNPs
        List
            x-axis tick labels
        List
            y-axis tick labels
        """
        xlabel = []
        for item in list(self.total_snps):
            xlabel.append(str(item))  
        x = []
        ylabel = []
        for consensus_file in list(rtre.get_tip_labels())[::-1]:
            basename = consensus_file.split(".")[0]
            values = self.snps[basename]
            sub_snps = [0]*len(self.total_snps)
            for value in values:
                sub_snps[list(self.total_snps).index(value)] = 1
            x.append(sub_snps)
            ylabel.append(consensus_file)
        # Flip array in the up/down direction
        matrix = np.flipud(np.asarray(x))
        return matrix, xlabel, ylabel

    def plot_snp_heatmap(
            self,
            rtre,
            min_AF=MIN_AF,
            render_type=None,
            width=400,
            height=None,
            legend_bounds=None,
            metadata_path=None,
            column_name=None):
        """
        Plot a phylogenetic tree and SNP heat map.
        Heat map legend:
            Blue = Consensus sequence matches the reference at position x.
            Red = Consensus sequence differs from the reference at position x.

        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        min_AF : float, optional
            Minimum allele frequency to detect an SNP
        render_type : {None, "html", "svg"}, optional
            Type of file to save the plot as
        width : int or None, optional
            Width of the tree in px
        height : int or None, optional
            Height of the tree in px
        legend_bounds : (xmin, xmax, ymin, ymax) tuple, optional
          Use the bounds property to position / size the image by specifying the
          position of each of its boundaries.  Assumes CSS pixels if units
          aren't provided, and supports all units described in :ref:`units`,
          including percentage of the canvas width / height.
          Ex. ("10%", "-10%", "75%", "90%")
        metadata_path : str
            Path to TSV file containing sample metadata (cluster names, etc)
        column_name : str
            Name of the column containing metadata
        """
        matrix, xlabel, _ = self._construct_consensus_matrix(rtre)
        # phylogenetic tree + heatmap visualization
        # scale tree to be 2X length of number of matrix cols
        ctree = rtre.mod.node_scale_root_height(matrix.shape[1]*2)
        # get canvas and axes with tree plot
        # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
        # 29 samples: height = 480 px
        # 35 SNPs: width = 900 px
        nnodes = matrix.shape[0]
        ncols = len(xlabel)
        # Use proportions to calculate the height of the canvas.
        # A tree with 29 nodes is 480 px tall.
        height = height or 480 * nnodes / 29
        x_ticks_height = 70
        ax0_width = width # tree width
        # A heat map with 35 SNPs is 440 px wide.
        ax1_x_start = ax0_width + 30 
        ax1_width = 440 * ncols / 35
        wid_end = int(ax1_x_start + ax1_width + 150)
        canvas = toyplot.Canvas(width=wid_end, height=height)
        #bounds (left,right,bottom,top)
        ax0 = canvas.cartesian(bounds=(0, ax0_width, 70, height - x_ticks_height + 13),)
        # subtract 10 from wid_end to prevent the scatterplot from being cut off.
        ax1 = canvas.cartesian(bounds=(ax1_x_start + 20, wid_end-10, 80,height - x_ticks_height))
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        #print(len(colors),len(sizes),len(nlabel))
        ctree.draw(node_colors=colors, node_sizes=sizes,node_labels=nlabel,\
                axes=ax0, tip_labels=True, tip_labels_align=True)
        ax0.show = False
        # Add metadata column
        if metadata_path:
            metadata_legend = {}
            metadata = {}
            file = open(metadata_path)
            read_tsv = csv.reader(file, delimiter="\t")
            column_idx = -1 # Line 1
            count = 0 # Determines which color to use for a cluster
            groups = {} # group_id => "count" value
            for line in read_tsv:
                if column_idx == -1:
                    # Find the desired column
                    column_idx = line.index(column_name)
                else:
                    basename = line[0].strip()
                    group_id = line[column_idx]
                    metadata[basename] = group_id
                    if group_id not in groups.keys():
                        count += 1
                        groups[group_id] = count
                    metadata_legend[group_id] = pallete[len(pallete) - groups[group_id]]
            col_colors = []
            for consensus_file in list(rtre.get_tip_labels()):
                basename = consensus_file.split(".")[0]
                if "ref" in basename: # reference
                        col_colors.append("#ffffff")
                else:
                    group_id = metadata[basename]
                    col_colors.append(metadata_legend[group_id])
            ax1.scatterplot(
                np.repeat(0, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
            # Create legend for metadata
            legend_bounds_temp = list(legend_bounds)
            metadata_legend_length = len(metadata_legend)
            new_height = ((legend_bounds[3] - legend_bounds[2]) * metadata_legend_length) / (metadata_legend_length + 2)
            legend_bounds_temp[3] = legend_bounds[2] + new_height
            new_legend_bounds = tuple(legend_bounds_temp)
            # recalculate legend_bounds for the "heatmap" legend
            legend_bounds_temp = list(legend_bounds)
            padding = min(new_height * 1.2, 50)
            legend_bounds_temp[2] = legend_bounds[2] + new_height + padding
            legend_bounds_temp[3] = legend_bounds[3] + padding
            legend_bounds = tuple(legend_bounds_temp)
            markers = []
            for group_id, color in metadata_legend.items():
                markers.append((group_id, toyplot.marker.create(shape="s", mstyle={"fill":color})))
            canvas.legend(markers,
                bounds=new_legend_bounds,
                label=column_name
            )
        # add n columns of data to the heatmap
        blue_shade = "#4477AA"
        red_shade = "#CC3311"
        plt = [blue_shade,red_shade]
        offset = 0
        if metadata_path:
            offset = 1
        for col in range(ncols):
            # select the column of data
            data = matrix[:, col]
            col_colors = [plt[i] for i in list(data)]
            # plot the data column
            ax1.scatterplot(
                np.repeat(col + offset, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
        ax1.x.ticks.show = True
        ax1.x.ticks.labels.angle = 90
        ax1.x.ticks.locator = toyplot.locator.Explicit(range(offset, len(xlabel) + offset),xlabel)
        ax1.y.show = False
        # https://toyplot.readthedocs.io/en/stable/markers.html#markers
        marker_a = toyplot.marker.create(shape="s", mstyle={"fill":blue_shade})
        marker_b = toyplot.marker.create(shape="s", mstyle={"fill":red_shade})
        # https://toyplot.readthedocs.io/en/stable/labels-and-legends.html
        canvas.legend([
                ("Reference", marker_a),
                ("SNP", marker_b),
            ],
            bounds=legend_bounds,
            label="Heatmap"
        )
        # stretch domain to fit long tip names
        #axes.x.domain.max =5 # fix this
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "consensus", "snp_heatmap.html")
            toyplot.html.render(canvas, file_path)
            print("Tree + SNP heatmap visualization has been saved to {}.".format(file_path))
        if render_type == "svg":
            file_path = os.path.join(self.output_directory, "consensus", "snp_heatmap.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree + SNP heatmap visualization has been saved to {}.".format(file_path))
        #plt.show()
        
    def infer_phylogeny(self, fasta_files, output_dir, threads=THREADS):
        """
        Infer the phylogeny of a group of sequences given their FASTA files.

        Parameters
        ----------
        fasta_files: str
            Path to folder of FASTA files to align
        output_dir: str
            Path to folder to place Parsnp and RAxML outputs into
        threads: int, optional
            Number of threads to use when running Parsnp and RAxML

        Returns
        -------
        str
            Path to the tree file generated by RAxML
        str
            Name of the reference sequence file
        """
        Path(os.path.join(output_dir, "parsnp")).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(output_dir, "raxml")).mkdir(parents=True, exist_ok=True)

        parsnp = os.path.join(output_dir, 'parsnp')
        
        tmpdata = FastaAligner(fasta_files)
        tmpdata.align(self.reference_fasta_path, output_dir=parsnp, threads=threads)
        # Parse multifasta
        multifasta = os.path.join(parsnp, 'parsnp.mfa')
        seqs = MultiFastaParser(multifasta)
        # Assume ref seq is first record in multifasta (is the case w/ parsnp)
        refname = seqs.records[0].name.split(".")[0]
        seqs.infer_phylogeny(output_dir=os.path.join(output_dir, "raxml"), threads=threads)
        raxml_newick = os.path.join(output_dir, "raxml", "RAxML_bestTree.tree")
        return raxml_newick, refname

    def _construct_isnv_matrix(self, rtre, min_AF=MIN_AF, show_snps=False):
        """
        Creates matrix that is the numerical representation of the iSNV heat map.
        
        Parameters
        ----------
        rtre : ToyTree
            Tree constructed from iSNV files
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        show_snps : boolean, optional
            Whether to set the matrix equal to 1 for iSNVs that have reached
            consensus in a sample. This will cause the heatmap to display a 
            red square for that sample at that iSNV.

        Returns
        -------
        List
            Matrix of allele frequencies where rows correspond to nodes
            and columns to iSNVs
        List
            All iSNVs within the sequences that make up the tree nodes
        """
        x, total_isnvs, file_isnvs = [],set(),defaultdict(list)
        files = []
        for f in list(rtre.get_tip_labels())[::-1]: 
            basename = f.split(".")[0] 
            files.append(basename)
        for basename in files:
            if "ref" not in basename:
                vcf_dict = extract_variants_from_vcf(os.path.join(self.vcf_directory, basename+".vcf"))
                #print(f"{basename}")
                for pos,nts in vcf_dict.items():
                    highest_freq = 0
                    max_freq = max([info[0] for nt, info in nts.items()])
                    kept_var = None
                    for nt, info in nts.items():
                        freq = info[0]
                        ref = self.reference[int(pos)-1]
                        #low frequency variants
                        if freq > highest_freq and freq>min_AF and freq != max_freq and nt != ref:
                            highest_freq = freq
                            kept_var = nt
                    if kept_var:
                        #lowfreq: pos,nucleotide
                        trie = Variant(pos, self.reference[int(pos)-1], kept_var, highest_freq)
                        total_isnvs.add(trie)
                        # isnv for each file: (pos&nucleotide, frequency)
                        #print(f"\t(position {pos}): Chose {trie.str2()} out of: {nts}")
                        file_isnvs[basename].append(trie)
        total_isnvs = sorted(list(total_isnvs), key=lambda x:(x.pos, x.ref, x.alt)) 
        for f in list(rtre.get_tip_labels())[::-1]:
            basename = f.split(".")[0].replace("'","")
            if basename in file_isnvs.keys():
                # row heatmap
                sub_x = [0]*len(total_isnvs)
                for isnv in file_isnvs[basename]:
                    sub_x[list(total_isnvs).index(isnv)] = isnv.freq
                # TODO(ahwang): This caused a LOT of cells to be red in the malta data.
                if show_snps:
                    for snp in self.snps[basename]:
                        # Check if any of the sample's SNPs are actually iSNVs in other samples
                        # If so, set the matrix value to 1
                        if snp in total_isnvs:
                            sub_x[list(total_isnvs).index(snp)] = 1
                x.append(sub_x)
            else:
                x.append([0]*len(total_isnvs))
        
        matrix = np.asarray(x)
        return matrix, total_isnvs
    
    def plot_isnv_heatmap(
            self, 
            rtre, 
            min_AF=MIN_AF, 
            show_snps=False,
            render_type=None, 
            width=500, 
            height=None,
            legend_bounds=("10%", "-10%", "75%", "90%"),
            metadata_path=None,
            column_name=None
    ):
        """
        Plot a phylogenetic tree and iSNV heat map.
        Heat map legend:
            Grey = the iSNV is not present in the sample
            Red = the iSNV is the majority allele in the sample
            Green -> Yellow = direction of increasing allele frequency of the iSNV
        
        Parameters
        ----------
        rtre : Toytree
            Tree to plot
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        show_snps : boolean, optional
            Whether to set the matrix equal to 1 for iSNVs that have reached
            consensus in a sample. This will cause the heatmap to display a 
            red square for that sample at that iSNV.
        render_type : {"html", "svg"}, optional
            Type of file to save the plot as
        width : int or None, optional
            Width of the tree in pixels
        height : int or None, optional
            Height of the tree in pixels
        legend_bounds : (xmin, xmax, ymin, ymax) tuple, optional
          Use the bounds property to position / size the image by specifying the
          position of each of its boundaries.  Assumes CSS pixels if units
          aren't provided, and supports all units described in :ref:`units`,
          including percentage of the canvas width / height.
        metadata_path : str
            Path to TSV file containing sample metadata (cluster names, etc)
        column_name : str
            Name of the column containing metadata
        """
        isnv_matrix, total_isnvs = self._construct_isnv_matrix(rtre, min_AF, show_snps=show_snps)
        xlabel = []
        for isnv in total_isnvs:
            xlabel.append(str(isnv))
        # store color list with values for tips and root
        colors = rtre.get_node_values('color', show_root=1, show_tips=1)
        sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
        nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
        # draw tree with node colors
        # scale tree to be 2X length of number of matrix cols
        ctree = rtre.mod.node_scale_root_height(isnv_matrix.shape[1]*2)
        # get canvas and axes with tree plot
        # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
        nnodes = isnv_matrix.shape[0]
        ncols = isnv_matrix.shape[1]
        # Use proportions to calculate the height of the canvas.
        height = height or 480 * nnodes / 29 # A tree with 29 nodes is 480 px tall.
        x_ticks_height = 70 # Height of the x-axis labels (iSNVs)
        ax0_width = width # Width of the tree
        ax1_x_start = ax0_width + 30 # Allow extra space for tip labels
        ax1_width = 885 * ncols / 55 # A heat map with 55 iSNVs is ~ 885 px wide.
        wid_end = int(ax1_x_start + ax1_width + 150)
        padding_for_legend = 100
        canvas = toyplot.Canvas(width=wid_end + padding_for_legend, height=height)
        # bounds (left,right,bottom,top)
        # Add 13 to ymax to "stretch" the tree to align with the heatmap.
        ax0 = canvas.cartesian(bounds=(0, ax0_width, 70, height - x_ticks_height + 13))
        # Subtract 10 from wid_end to prevent the scatterplot from being cut off.
        ax1 = canvas.cartesian(bounds=(ax1_x_start + 20,wid_end-10, 85, height - x_ticks_height))
        ###### TREE ######
        ctree.draw(node_colors=colors, node_sizes=sizes,node_labels=nlabel,\
                axes=ax0, tip_labels=True, tip_labels_align=True)
        ax0.show = False
        ###### METADATA LAYER #####
        if metadata_path:
            metadata_legend = {}
            metadata = {}
            file = open(metadata_path)
            read_tsv = csv.reader(file, delimiter="\t")
            column_idx = -1 # Line 1
            count = 0 # Determines which color to use for a cluster
            groups = {} # group_id => "count" value
            for line in read_tsv:
                if column_idx == -1:
                    # Find the desired column
                    column_idx = line.index(column_name)
                else:
                    basename = line[0].strip()
                    group_id = line[column_idx]
                    metadata[basename] = group_id
                    if group_id not in groups.keys():
                        count += 1
                        groups[group_id] = count
                    metadata_legend[group_id] = pallete[len(pallete) - groups[group_id]]
            col_colors = []
            for isnv_file in list(rtre.get_tip_labels()):
                basename = isnv_file.split(".")[0]
                if "ref" in basename: # reference
                        col_colors.append("#ffffff")
                else:
                    group_id = metadata[basename]
                    col_colors.append(metadata_legend[group_id])
            ax1.scatterplot(
                np.repeat(0, rtre.ntips),
                np.arange(rtre.ntips),
                marker='s',
                size=9,
                color=col_colors)
            # Create legend for metadata
            legend_bounds_temp = list(legend_bounds)
            metadata_legend_length = len(metadata_legend)
            new_height = ((legend_bounds[3] - legend_bounds[2]) * metadata_legend_length) / (metadata_legend_length + 2)
            legend_bounds_temp[3] = legend_bounds[2] + new_height
            new_legend_bounds = tuple(legend_bounds_temp)
            # recalculate legend_bounds for the "heatmap" legend
            legend_bounds_temp = list(legend_bounds)
            padding = min(new_height * 1.2, 50)
            legend_bounds_temp[2] = legend_bounds[2] + new_height + padding
            legend_bounds_temp[3] = legend_bounds[3] + padding
            legend_bounds = tuple(legend_bounds_temp)
            markers = []
            for group_id, color in metadata_legend.items():
                markers.append((group_id, toyplot.marker.create(shape="s", mstyle={"fill":color})))
            canvas.legend(markers,
                bounds=new_legend_bounds,
                label=column_name
            )
        ###### HEATMAP ######
        ncols = len(total_isnvs)
        colormap = toyplot.color.brewer.map("GreenYellow") # Used to be GreenYellowRed, BlueYellowRed
        defaultmap = toyplot.color.brewer.map("Set1")
        default_color = "#000000" # formerly dark grey: #888888"
        offset = 0
        if metadata_path:
            offset = 1
        for col in range(ncols):
            # select the column of data
            data = isnv_matrix[:, col][::-1]
            #print(f"Column {col} data: {data}")
            col_colors = []
            for i in list(data):
                if i == 0:
                    col_colors.append(toyplot.color.css(default_color)) 
                elif i == 1:
                    # red if the shared lofreq variant IS the majority allele for this sample
                    col_colors.append(defaultmap.colors(0,0,1)) 
                else:
                    col_colors.append(colormap.colors(math.log(i + .01),math.log(0.01),math.log(0.5)))
            # plot the data column
            ax1.scatterplot(
                np.repeat(col + offset, rtre.ntips), # repeating X-value
                np.arange(rtre.ntips), # Y-values
                marker='s',
                size=10, 
                color=col_colors)
            ax1.x.ticks.show = True
            ax1.x.ticks.labels.angle = 90
            ax1.x.ticks.locator = toyplot.locator.Explicit(range(offset, len(xlabel) + offset),xlabel)
            ax1.y.show = False
            # stretch domain to fit long tip names
            # axes.x.domain.max =10 # ahwang: fix this
        colors = [default_color] + list(toyplot.color.brewer.map("GreenYellow")._palette._colors) + [defaultmap.colors(0)]
        full_palette=toyplot.color.Palette(colors)
        full_colormap = toyplot.color.LinearMap(
            palette=full_palette,
            domain_min=0,
            domain_max=1
        )
        ticklocator = toyplot.locator.Explicit(
            locations=[0,.9,1],
            labels=["0", ".5", "1"]
        )
        canvas.color_scale(
                    full_colormap, 
                    bounds=legend_bounds, 
                    label="Allele Frequency",
                    ticklocator=ticklocator)
        # ax1.color_scale(full_colormap, 
        #                 label="Allele Frequency",
        #                 tick_locator=ticklocator
        # )
        if render_type == "html":
            file_path = os.path.join(self.output_directory, "isnv", "isnv_heatmap.html")
            toyplot.html.render(canvas, file_path)
            print("Tree + iSNV heat map visualization has been saved to {}.".format(file_path))
        elif render_type == "svg":
            file_path = os.path.join(self.output_directory, "isnv", "isnv_heatmap.svg")
            toyplot.svg.render(canvas, file_path)
            print("Tree + iSNV heat map visualization has been saved to {}.".format(file_path))
            
    def align_isnv_clusters(self, threads=THREADS):
        """
        Infer the phylogeny of iSNV embedded sequences by sample cluster.

        Parameters
        ----------
        threads : int, optional
            Number of threads to use when running Parsnp and RAxML

        Returns
        -------
        str
            Path to folder containing one folder of tree files per cluster.
        """
        # If metadata is present:
        # 	Create a collection of folders, each representing a cluster based on the metadata. 
        # 	Copy over these iSNV embedded sequences into each folder.
        Path(os.path.join(self.output_directory, "isnv", "groups")).mkdir(parents=True, exist_ok=True)
        groups_isnv = defaultdict(list)
        if self.metadata_cluster_assignments:
            for basename, cluster in self.metadata_cluster_assignments.items():
                groups_isnv[cluster].append(basename+".isnv.fasta")
        else:
            # Create groups based on same sequence.
            for basename, group in self.group_assignments.items():
                groups_isnv[group].append(basename + ".isnv.fasta")
        # Copy over consensus iSNV FASTA files to new folders based on the cluster
        for group_id, files in groups_isnv.items():
            #if len(files) > 1:
            for filename in files:
                # TODO: Delete folder if it exists?
                path = os.path.join(self.output_directory, "isnv", "groups", str(group_id))
                Path(path).mkdir(parents=True, exist_ok=True)
                # copy the 'TRANSM-38.isnv.fasta' files from consensus_isnv/ folder to consensus_isnv_group/%s/%s
                os.system("cp %s %s"%(os.path.join(self.output_directory, "isnv", "sequences", filename), os.path.join(self.output_directory,"isnv", "groups", str(group_id), filename)))
        output_dir_isnv = os.path.join(self.output_directory, "isnv")
        for group in os.listdir(os.path.join(output_dir_isnv, "groups")):
            if os.path.isdir(os.path.join(output_dir_isnv, "groups", group)):		
                consensus_isnv_address = os.path.join(output_dir_isnv, "groups", group)
                self.infer_phylogeny(
                    consensus_isnv_address, 
                    os.path.join(self.output_directory, "isnv", "groups", group),
                    threads=threads
                )
        return os.path.join(output_dir_isnv, "groups")

    def plot_isnv_clusters(
            self,
            cluster_folder_path,
            min_AF=MIN_AF,
            render_type=None,
            show_legend=False):
        """
        Plot the phylogenetic tree and iSNV heat map of each cluster.

        Parameters
        ----------
        cluster_folder_path : str
            Path to folder containing one folder of tree files per cluster.
            The tree files have to be in a folder titled "raxml",
            i.e. "{cluster_folder_path}/A/raxml"
        min_AF : float, optional
            Minimum allele frequency to detect an iSNV
        render_type : {None, "html", "svg"}, optional
            Type of file to save the plots as
        show_legend : boolean, optional
            Whether to display the legend mapping colors to iSNV frequencies
        """
        for cluster in os.listdir(cluster_folder_path):
            filename = os.path.join(cluster_folder_path, cluster, "raxml", "RAxML_bestTree.tree")
            if not os.path.isfile(filename):
                print("{} does not exist!".format(filename))
                continue
            rtre = self.construct_tree(tree_path=filename, root_wildcard="SARS")
            matrix, total_isnvs = self._construct_isnv_matrix(rtre, min_AF=MIN_AF)
            # scale tree to be 2X length of number of matrix cols
            rtre = rtre.mod.node_scale_root_height(matrix.shape[1]*1.5)
            colors = rtre.get_node_values('color', show_root=1, show_tips=1)
            sizes = rtre.get_node_values('size', show_root=1, show_tips=1)
            nlabel = rtre.get_node_values('nlabel', show_root=1, show_tips=1)
            num_nodes = len(list(rtre.get_tip_labels()))
            # get canvas and axes with tree plot
            # set up a toyplot Canvas with 2 axes: (x1, x2, y1, y2)
            wid_end = 300+ 15*len(total_isnvs)
            hght = 35*num_nodes 
            x_ticks_height = 70 # Height of the x-axis labels (iSNVs)
            padding_for_legend = 0
            if show_legend:
                padding_for_legend = 50
            canvas = toyplot.Canvas(width=wid_end, height=hght + x_ticks_height + padding_for_legend)
            #bounds (left,right,bottom,top)
            ax0_height = hght - x_ticks_height
            ax0 = canvas.cartesian(bounds=(0, 300, 0, ax0_height), padding=5, ymin=0, ymax=num_nodes+2)
            ax1 = canvas.cartesian(bounds=(300+30,wid_end-10, 0,hght - x_ticks_height), padding=10, ymin=0, ymax=num_nodes+2)
            rtre.draw(axes=ax0, tip_labels=True, tip_labels_align=True, node_colors=colors, node_sizes=sizes,node_labels=nlabel)
            ax0.show = False
            # add n columns of data (here random data)
            ncols = len(total_isnvs)
            colormap = toyplot.color.brewer.map("GreenYellow")
            defaultmap = toyplot.color.brewer.map("Set1")
            for col in range(ncols):
                # select the column of data
                data = matrix[:, col][::-1]
                col_colors = []
                for i in list(data):
                    if i == 0 :
                        col_colors.append(toyplot.color.css("#888888"))
                    elif i == 1:
                        col_colors.append(defaultmap.colors(0,0,1))
                    else:
                        col_colors.append(colormap.colors(math.log(i+0.01),math.log(0.01),math.log(0.5)))
                # plot the data column
                ax1.scatterplot(np.repeat(col, rtre.ntips), # repeating X-value
                                np.arange(rtre.ntips), # Y-values
                                marker='s',
                                size=10, 
                                color=col_colors)
            ax1.x.ticks.show = True
            ax1.x.ticks.labels.angle = 90
            ax1.x.ticks.locator = \
            toyplot.locator.Explicit(range(len(total_isnvs)),total_isnvs)
            ax1.y.show = False
            if show_legend:
                colors = ["#888888"] + list(toyplot.color.brewer.map("GreenYellow")._palette._colors) + [defaultmap.colors(0)]
                full_palette=toyplot.color.Palette(colors)
                full_colormap = toyplot.color.LinearMap(
                    palette=full_palette,
                    domain_min=0,
                    domain_max=1
                )
                canvas.color_scale(
                    full_colormap, 
                    bounds=("10%", "25%", ax0_height, ax0_height + 50), # legend_bounds, 
                    label="Allele Frequency")
            if render_type == "html":
                toyplot.html.render(canvas, os.path.join(cluster_folder_path, "{}.html".format(cluster)))
            if render_type == "svg":
                toyplot.svg.render(canvas, os.path.join(cluster_folder_path, "{}.svg".format(cluster)))