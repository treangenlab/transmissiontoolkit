### IMPORTS ###
import os
import subprocess
from subprocess import Popen, PIPE
import shutil

from pathlib import Path
from collections import defaultdict

# Using package resources
import importlib
import importlib.resources as resources

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

def _add_executable_path(name):
    """
    Returns path to the trakit package's executable if `name` is not on $PATH.

    Parameters
    ----------
    name : str
        Name of the executable, ex. "raxmlHPC"
    
    Returns
    -------
    str
        Path to the executable in the package resources if it is not on $PATH.
        Otherwise, the original "name" input.
    """
    if shutil.which(name) is None:
        with resources.path('trakit.executables', name) as executable:
            return executable
    return name

def move_empty_files(path, empty_vcf_list):
    """Move a provided list of VCF files to a directory."""
    for filename in empty_vcf_list:
        shutil.move(filename, path)
        
def parse_mask_file(masks):
    """
    Parses a text file of genomic positions to mask.

    Parameters
    ----------
    masks : str
        Path to a text file of comma-separated integers
    
    Returns
    -------
    list
        list of integers read from the file
    """
    mask_positions = []
    with open(masks, "r") as mask_file:
            for line in mask_file:
                comma_split = line.split(",")
                for item in comma_split:
                    nums = item.split("-")
                    if len(nums) == 1:
                        mask_positions.append(int(nums[0]))
                    else:
                        for num in range(int(nums[0]), int(nums[1])):
                            mask_positions.append(int(num))
    return mask_positions
        
def count_shared_variants(transm_pairs_list, mask_pairs_list=None, high_shared=6):
    """
    Counts the number shared variants in all possible pairs.

    Parameters
    ----------
    transm_pairs_list : list
        list of TransmissionPair instances
    mask_pairs_list : list, optional
        list of TransmissionPair instances after masking
    high_shared : int, optional
        minimum number of variants a pair of samples must share to be "important"

    Returns
    -------
    dict
        If mask_pairs_list is not provided:

                {
                    num shared variants: num pairs with that num shared variants
                }

        If mask_pairs_list is provided:

                {   
                    num shared variants: [
                        num pairs with that num shared variants,
                        num pairs with that num shared variants after masking
                    ]
                }
    List
        List of pairs that meet the threshold amount of shared variants
    """
    def masked_default_value():
        return [0,0]
    if mask_pairs_list:
        sv_count = defaultdict(masked_default_value)
    else:
        sv_count = defaultdict(int) # tracks number of pairs with a given count.
    important_pairs = []
    #Unmasked list
    for transm_pair in transm_pairs_list:
        # mapping of site => variant => frequency/depth information
        # The old logic assumed that a variant is shared whenever recip freq != 0.
        # Thus, it always made a pass through TransmissionPair.variants.
        # However, we can just rely on TransmissionPair.num_shared attribute.
        pair_sv = transm_pair.num_shared
        if not mask_pairs_list:
            sv_count[pair_sv] += 1
        else: 
            sv_count[pair_sv][0] += 1
        if pair_sv > high_shared: # TODO: high_shared = threshold
            important_pairs.append((transm_pair.donor_basename, transm_pair.recipient_basename))
    # At this point, we can guarantee that every value in sv_count mapping is of the same format. They're all either a number or they're a list of 2 numbers.
    #Masked list
    if mask_pairs_list:
        # We can guarantee that every value in sv_count mapping is a list of 2 numbers.
        for transm_pair in mask_pairs_list:
            mask_pair_sv = transm_pair.num_shared
            if mask_pair_sv in sv_count:
                sv_count[mask_pair_sv][1] += 1
            if mask_pair_sv > high_shared:
                important_pairs.append((transm_pair.donor_basename, transm_pair.recipient_basename))
    return sv_count, important_pairs

def position_counter(transm_pairs):
    """
    Counts number of pairs that have variants at each position

    Parameters
    ---------- 
    transm_pairs : list
        A list of TransmissionPair instances
    
    Returns
    -------
    dict
        {
            position: number of pairs with a variant at that position
        }
    """
    position_count = defaultdict(int)
    for transm_pair in transm_pairs:
        variants = transm_pair.variants
        for pos, var_mapping in variants.items():
            for var in var_mapping:
                if var_mapping[var][1] != 0:
                    position_count[pos] += 1
    return position_count
        
############ UTILITY CLASSES ############
class TransmissionPair:
    """
    Representation of a pair of samples.

    Attributes
    ----------
    donor_basename : str
        Base name of the donor sample file
    recipient_basename : str
        Base name of the recipient sample file
    variants : dict
        Variant frequency and depth information

            {
                site (int): {
                    variant (str): [
                        donor frequency (float), 
                        recipient frequency (float), 
                        donor depth (int), 
                        recipient depth (int)
                    ]
                }
            }

    num_shared : int
        Number of variants shared by the pair
    """
    def __init__(self, donor_basename, recipient_basename, variants, num_shared):
        self.donor_basename = donor_basename
        self.recipient_basename = recipient_basename
        self.variants = variants
        self.num_shared = num_shared

class FastaAligner:
    """
    Class for aligning FASTA files.

    This class is meant for handling FASTA files with one sequence per file, and will raise
    an error if given a multi-FASTA file.
    """
    def __init__(self, fasta_dir):
        """
        Parameters
        ----------
        fasta_dir : str
            Path to a directory of FASTA files
        """
        if os.path.isdir(fasta_dir):
            self.dir = fasta_dir
        else:
            raise ValueError("The specified path should be a directory of fasta files.")

    def align(self, reference, output_dir='', threads=THREADS, verbose=False):
        """
        Aligns all FASTA files in a directory.

        Parameters
        ----------
        reference : str
            Path to reference genome FASTA file
        output_dir : str, optional
            Path to directory to store Parsnp and HarvestTools outputs in
        threads : int, optional
            Number of threads to use when running Parsnp
        verbose : boolean, optional
            Whether to print the command(s) being executed
        """
        if not os.path.exists(reference):
            raise FileNotFoundError(f"{reference}")
        Path(output_dir).mkdir(parents=True, exist_ok=True)       
        output = os.path.join(os.getcwd(), output_dir)
        # -d: (d)irectory containing genomes/contigs/scaffolds
        # -r: (r)eference genome (set to ! to pick random one from genome dir)
        # -o: output directory? default [./P_CURRDATE_CURRTIME]
        # -p: number of threads to use? (default= 1)
        cmnd = f"parsnp -d {self.dir} -r {reference} -o {output} -p {threads}"
        if verbose:
            print("cmnd: " + cmnd)
        subprocess.call(cmnd.split())
        path2xmfa = os.path.join(output, 'parsnp.xmfa')
        # -x: xmfa alignment file
        # -M: <multi-fasta alignment output (concatenated LCBs)>
        cmnd2 = f"harvesttools -x {path2xmfa} -M " + os.path.join(output_dir,'parsnp.mfa')
        if verbose:
            print("cmnd: " + cmnd2)
        subprocess.call(cmnd2.split())        
        
class FastaRecord:
    """
    Simple class for storing and accessing data from a FASTA record.

    Attributes
    ----------
    name : str
        Name of the sequence
    sequence : str
        The sequence
    """
    def __init__(self, name, sequence):
        if not isinstance(name, str):
            raise TypeError('Name should be a string.')
        if not isinstance(sequence, str):
            raise TypeError('Sequence should be a string.')
        self.name = name
        self.seq = sequence
    
    def set_id(self, identifier):
        """
        identifier : str, int, or float
            ID of this record
        """
        if not isinstance(identifier, (str, int, float)):
            raise TypeError('ID should be a string, integer, or float.')
        self.id = identifier
        
class MultiFastaParser:
    """
    Class for storing and accessing data from a multi-FASTA file.

    Attributes
    ----------
    multifasta : str
        Path to a multi-FASTA file
    """
    def __init__(self, multifasta):
        if os.path.isfile(multifasta):
            self.fasta = multifasta
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), multifasta) 
        self.records = []
        # Populates self.records with FastaRecord objects
        with open(self.fasta, 'r') as f:
            lines = [line.strip() for line in f.readlines()]
            length = len(lines)
            for i in range(0, length - 1):
                if lines[i][0] == '>':
                    j = 1
                    while i+j < length:
                        if lines[i+j][0] == '>':
                            break
                        j += 1
                    consensus = ''.join(line for line in lines[i+1:i+j])
                    name = lines[i][1:]
                    self.records.append(FastaRecord(name, consensus))

    def get_groups(self):
        """
        Returns mapping of sequence to names of records with that sequence.
        """
        groups = dict()
        for record in self.records:
            if record.seq in groups:
                groups[record.seq].add(record.name)
            else:
                groups[record.seq] = {record.name}
        return list(groups.values())

    def infer_phylogeny(
            self, 
            output_dir='', 
            label='tree', 
            threads=THREADS, 
            custom_cmd='', 
            verbose=False
    ):
        """
        Runs RAxML on the multi-FASTA file and stores output in output_dir.

        Parameters
        ----------
        output_dir : str, optional
            Path to a directory to save the generated tree file(s) in
        label : str, optional
            Specifies the name of the output file.
        threads : int, optional TODO(ahwang): threads or random number seed?
            Number of threads to use when running RAxML
        custom_cmd : str, optional
            Command to run inplace of the default RAxML command
        verbose : boolean, optional
            Whether to print the command being executed
        """
        # If directory already exists, delete all files with same label
        if output_dir:
            if os.path.exists(output_dir):
                for fname in os.listdir(output_dir):
                    if fname.endswith(f".{label}"):
                        os.remove(os.path.join(output_dir, fname))
            else:
                Path(output_dir).mkdir(parents=True, exist_ok=True)
        # If user specifies a command, then run that instead
        if custom_cmd:
            cmnd = custom_cmd
        # Otherwise, run RaxML with default arguments
        else:
            cwd = os.getcwd()
            path = os.path.join(cwd, self.fasta)
            # -s: Specify the name of the alignment data file in PHYLIP format
            # -w: FULL (!) path to the directory into which RAxML shall write its output files (DEFAULT: current directory)
            # -n: Specifies the name of the output file.
            # -m: Model of Binary (Morphological), Nucleotide, Multi-State, or Amino Acid Substitution 
            # -p: Specify a random number seed for the parsimony inferences. This allows you to reproduce your results and will help me debug the program.
            raxmlHPC_path = _add_executable_path('raxmlHPC')
            cmnd = f"{raxmlHPC_path} -s {path} -w {os.path.join(cwd, output_dir)} \
                -n {label} -m GTRCAT -p 12345" #  -p {threads}
            if verbose:
                print(cmnd)
        subprocess.call(cmnd.split())
        # Print out when finished
        if verbose: 
            msg = f'Ran RAxML on {self.fasta} and stored files in directory: {output_dir}' + '.'
            print(msg)

class Variant:
    """
    Representation of one single nucleotide polymorphism.

    Attributes
    ----------
    pos: int
        reference position
    ref : str
        reference allele
    alt : str
        alternate allele
    freq : float
        alternate allele frequency
    """
    def __init__(self, pos, ref, alt, freq):
        self.pos = pos
        self.ref = ref
        self.alt = alt
        self.freq = freq
    
    def str2(self):
        return "%s%s%s (%s)"%(self.ref, self.pos, self.alt, self.freq)
    
    def __str__(self):
        return "%s%s%s"%(self.ref, self.pos, self.alt)
    
    def __eq__(self, other):
        return self.pos == other.pos and self.ref == other.ref and self.alt == other.alt

    def __lt__(self, other):
        return self.pos < other.pos or (self.pos == other.pos and self.alt < other.alt)
    
    def __hash__(self):
        return hash((self.pos, self.ref, self.alt))