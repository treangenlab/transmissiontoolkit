### IMPORTS ###
import os
import subprocess
from subprocess import Popen, PIPE

from trakit.trakitUtils import TransmissionPair
from trakit.vcfParsing import extract_variants_from_vcf, extract_shared_variants
from pathlib import Path

# Using package resources
import importlib
import importlib.resources as resources

################ CONSTANTS ###############
# Minimum allele frequency to detect an iSNV
MIN_AF = .02

THREADS = 1

LINE_WRAP = 80 # Max. length of each line in fasta file

COLORS = [
    '#DC050C', '#E8601C', '#F1932D', '#F6C141', '#F7F056', '#CAE0AB',
    '#90C987', '#4EB265', '#7BAFDE', '#5289C7', '#1965B0', '#882E72'
]

pallete = ['red', 'gold', 'deepskyblue', 'green','darkorchid', 'orange', \
           'palegreen','rosybrown','grey','wheat','royalblue',\
           'saddlebrown','salmon','peru','orchid','papayawhip','plum',\
            'antiquewhite', 'deeppink', 'mediumturquoise', 'yellowgreen',\
            'thistle', 'dodgerblue', 'crimson', 'burlywood', 'aquamarine']

################ CODE ################

def write_bb_bottleneck_input_file(
        donor,
        recipient,
        parse_type="biallelic",
        min_read_depth=0,
        min_AF=MIN_AF,
        max_AF=1,
        masks=None
):
    """
    Writes input file for BB bottleneck software.

    Parameters
    ----------
    donor : str
        Path to the donor VCF file
    recipient : str
        Path to the recipient VCF file
    parse_type: {'biallelic', 'multiallelic'} (unused)
        Handles the case of when there are multiple variants at the same position
    min_read_depth : int, optional
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    
    Returns
    -------
    str
        Path to .txt file formatted as an input to the BB bottleneck software.
    """
    # Parse data and store outputs
    transm_pair = extract_shared_variants(
        donor, 
        recipient, 
        parse_type=parse_type, 
        min_read_depth=min_read_depth, 
        min_AF=min_AF, 
        max_AF=max_AF, 
        masks=masks,
        store_donor_only_variants=True
    )
    variants = transm_pair.variants
    num_shared = transm_pair.num_shared
    # Remove directory names from file paths
    fname1 = os.path.basename(donor)
    fname2 = os.path.basename(recipient)
    # Remove file extensions
    fname1 = fname1.split(".")[0] 
    fname2 = fname2.split(".")[0]
    # Write data to file in BB_Bottleneck input format
    filename = f"new_{fname1}_{fname2}_thred{num_shared}_complete_nofilter_bbn.txt"
    with open(filename, "w") as f:
        for pos in variants:
            for var in variants[pos]:
                f.write(str(variants[pos][var][0])+'\t'+str(variants[pos][var][1])+'\n')
    return filename

def _estimate_bottleneck_size(
        donor_path,
        recipient_path,
        min_read_depth,
        min_AF,
        max_AF,
        min_shared,
        verbose=False,
        cluster_name=None,
        plot_bool=False,
        masks=None
):
    """
    Estimate the bottleneck size of a given transmission pair.

    Parameters
    ----------
    donor_path : str
        Path to a VCF file
    recipient_path : str
        Path to a VCF file
    min_read_depth : int
        Minimum read depth to validate a variant, by default 0
    min_AF : float or (float, float), optional
        Minimum allele frequency to validate a variant. 
        
        * float: Enforce the same minimum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different minimum AF on the donor and recipient
        samples, respectively.
    max_AF : float or (float, float), optional
        Maximum allele frequency to validate a variant. 
        
        * float: Enforce the same maximum AF on the donor and recipient samples
        when extracting variants. 
        * (float, float): Enforce different maximum AF on the donor and recipient
        samples, respectively.
    min_shared : int
        Minimum number of variants the pair must share
    verbose : boolean, optional
        Whether to print the command(s) being executed
    cluster_name : str, optional
        Name of the cluster that the samples belong to, if known.
    plot_bool : bool, optional
        Whether to produce the log likelihood vs bottleneck size plot
    masks : str, optional
        Path to a txt file containing comma-separated genomic sites to mask
    
    Returns
    -------
    List or None
        [
            cluster_name, 
            bottleneck_size, 
            left bound, 
            right bound]
        ]
    """
    transm_pair = extract_shared_variants(
        donor_path,
        recipient_path,
        min_read_depth=min_read_depth,
        min_AF=min_AF,
        max_AF=max_AF,
        masks=masks,
        store_donor_only_variants=True
    )
    num_shared = transm_pair.num_shared
    if num_shared < min_shared:
        return None
    filename = write_bb_bottleneck_input_file(donor_path,
        recipient_path,
        min_read_depth=min_read_depth,
        min_AF=min_AF,
        max_AF=max_AF,
        masks=masks
    )
    with resources.path('trakit.scripts', 'Bottleneck_size_estimation_approx.r') as bottleneck_script:
        bb_command = f"Rscript {bottleneck_script} --file {filename} --var_calling_threshold 0.0001 --Nb_min 1 --Nb_max 200 --confidence_level .95"
        # --plot_bool FALSE does not actually prevent the plot from being produced
        if plot_bool:
            bb_command += f" --plot_bool TRUE"
        if verbose:
            print(bb_command)
        proc = subprocess.Popen(bb_command, shell=True, stdout=subprocess.PIPE)
        outs, _ = proc.communicate() # Read data from stdout 
        outs = outs.decode()
        output_lst = outs.split("\n[1]")
        output_lst = [s.lstrip(" \r").rstrip("\n") for s in output_lst]
        if output_lst[0] == '[1] "Bottleneck size"':
            bottleneck_size = int(output_lst[1])
            left_bound_interval = int(output_lst[3])
            right_bound_interval = int(output_lst[5])
            cluster_name = cluster_name
            return [cluster_name, bottleneck_size, left_bound_interval, right_bound_interval]
        else:
            return None
    return None