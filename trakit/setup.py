import setuptools

setuptools.setup(
    name="trakit",
    version="0.0.4",
    author="Angela Hwang",
    author_email="alh9@rice.edu",
    description="Transmission Toolkit",
    packages=setuptools.find_packages(),
    install_requires=[
        'jupyter==1.0.0',
        'matplotlib==3.2.1',
        'numpy==1.19.2',
        'PyVCF==0.6.8',
        'toyplot==0.19.0',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    package_data={'trakit': ['scripts/*', 'executables/*']},
)